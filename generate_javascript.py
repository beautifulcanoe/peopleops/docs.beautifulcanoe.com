#!/usr/bin/env python2.7

"""
Generate Javascript to render some GitLab Markdown elements in HTML.

In GitLab, ~Bug and ~"Feature Request" are Markdown representations of project
labels. These are rendered in HTML as pill-badges. This script obtains a list
of all project labels from GitLab, then generates Javascript to render each
label correctly.

Similarly, @bc-bot is a user name, and on GitLab would be rendered as a link
to the profile of that user.
"""

import gitlab
import os

# Environment variables set in GitLab CI/CD configuration.
BC_BOT = os.environ['BC_BOT']
BC_CTO = os.environ['BC_CTO']
BC_TECH_LEAD = os.environ['BC_TECH_LEAD']
GITLAB_INSTANCE = os.environ['GITLAB_INSTANCE']
PERSONAL_ACCESS_TOKEN = os.environ['TOKEN']
# ID of this project. From Settings -> General in GitLab.
PROJECT_ID = os.environ['PROJECT_ID']
# FILE_PATH should also appear in .gitignore, so that we don't cache a version
# of the Javascript that may become out of date.
FILE_PATH = 'docs/assets/javascript/gitlab.js'
JS_FUNCTION = """
const replaceOnDocument = (pattern, string, {target = document.body} = {}) => {
  [
    target,
    ...target.querySelectorAll("*:not(script):not(noscript):not(style)")
  ].forEach(({childNodes: [...nodes]}) => nodes
    .filter(({nodeType}) => nodeType === document.TEXT_NODE)
    .forEach((textNode) =>
    {

          const doc = textNode.ownerDocument;
          const fragment = doc.createDocumentFragment();
          const div = doc.createElement('div');
          div.appendChild(document.createTextNode(textNode.textContent));
          div.innerHTML = div.innerHTML.replace(pattern, string);
          var [...replacements] = div.childNodes;
          replacements.forEach(node => fragment.appendChild(node));
          textNode.parentNode.replaceChild(fragment, textNode);

    }));
};

"""


def _make_button(name, colour, description):
    """Create HTML text for a single pill badge.
    """
    return ('<button class=\\"button\\" style=\\"background-color: {};\\">'
            '<div class=\\"tooltip\\">{}<span class=\\"tooltiptext\\">{}</span>'
            '</div></button>').format(colour, name, description)


def _make_label_replacement_rule(name, colour, description):
    """Create a single replacement rule, replacing mark-up with HTML.

    For example, to replace the ~Bug label with a pill badge:

        replaceOnDocument(/~Bug/g, "<button ...>Bug</button>");
    """

    text = ''
    # If name does not contain spaces, it may or may not be wrapped in
    # double-quotes. In this case, we replace both the quoted and unquoted
    # versions of the label mark-up.
    if ' ' not in name:
        text += 'replaceOnDocument(/~{}/g, "{}");\n'.format(name, _make_button(name, colour, description))
    text += 'replaceOnDocument(/~"{}"/g, "{}");\n'.format(name, _make_button(name, colour, description))
    return text


def _make_user_replacement_rule(user):
    """Create a single replacement rule, replacing mark-up for a user with HTML.

    For example, to replace the user @bc-bot with a link:

        replaceOnDocument(/@bc-bot/g, "<a href="..>">@bc-bot</a>");
    """

    profile = '<a href=\\"{}/{}\\">@{}</a>'.format(GITLAB_INSTANCE, user, user)
    return 'replaceOnDocument(/@{}/g, "{}");\n'.format(user, profile)


def _make_javascript(file_path, labels):
    """Create Javascript which replaces Markdown labels and users with HTML.
    """

    text = JS_FUNCTION
    for label in labels:
        text += _make_label_replacement_rule(label.name, label.color, label.description)
    text += _make_user_replacement_rule(BC_BOT)
    text += _make_user_replacement_rule(BC_CTO)
    text += _make_user_replacement_rule(BC_TECH_LEAD)
    return text


def _get_labels():
    """Return a list of all GitLab labels associated with this project.
    """

    project = gitlab.Gitlab(GITLAB_INSTANCE,
                            private_token=PERSONAL_ACCESS_TOKEN,
                            api_version=4).projects
    return project.get(PROJECT_ID, lazy=True).labels.list(all=True)


if __name__ == '__main__':
    fp = open(FILE_PATH, 'w')
    fp.write(_make_javascript(FILE_PATH, _get_labels()))
    fp.close()
