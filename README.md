# Beautiful Canoe Developer Handbook

[![pipeline status](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/badges/main/pipeline.svg)](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/-/commits/main)

A handbook for all sorts of things that happen at Beautiful Canoe.

This repository should be a one-stop shop for all *technical* Beautiful Canoe related documentation.
Technical documentation that is specific to an individual project should be found in the GitLab repository for that project.

This handbook contains information about technology, processes and conventions at Beautiful Canoe specifically.
We do not usually document generic information about languages and tech stacks (for example, tutorials on PHP or Laravel).
Languages and technologies evolve quickly, and you should refer to the official documentation for the particular version the stack you are using.

## If you are new to Beautiful Canoe

If you are just starting out, please follow [the first day HOWTO](https://docs.beautifulcanoe.com/start-here/howto-first-day.html).

## If your contact with Beautiful Canoe is about to end

We're sorry to see you go!
If it's your last week there are a [number of things](https://docs.beautifulcanoe.com/start-here/howto-last-week.html) we'd like you to do before you leave, just to make life easier for the next student developers who will be working on your project.

## Contributors (alphabetically by surname)

* Callum Bugajski - @callumbugajski
* Adam Collins - @1adamcollins1
* Antonio Garcia-Dominguez - @a.garcia-dominguez
* Johnny Le - @johnnyle
* Asim Mahmood - @asim1999
* Sarah Mount - @snim2
* David Powell - @pavedowell

## Making changes to Beautiful Canoe documentation

Please see [CONTRIBUTING.md](/CONTRIBUTING.md) for advice on how to contribute to this repository.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
