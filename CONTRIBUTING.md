# Contributing to the docs.beautifulcanoe.com repository

This repository contains a [mkdocs](https://www.mkdocs.org/) site, using the [Material](https://squidfunk.github.io/mkdocs-material/) theme, to serve our company documentation.

[[_TOC_]]

---

## Getting started

### Clone this repository

From the command line, run:

```shell
git clone git@gitlab.com:beautifulcanoe/peopleops/docs.beautifulcanoe.com.git
cd docs.beautifulcanoe.com
```

### Install Docker

If you are working on Ubuntu, this should work for you:

```shell
sudo apt-get install docker docker.io docker-compose
```

It is also a good idea to add yourself to the `docker` group, so that you can avoid using `sudo`:

```shell
sudo usermod -aG docker ${USER}
```

Before `usermod` takes effect, you will need to log out and log back in again.
After that, please check that this has worked by running `id -nG` and checking that `docker` is listed in your groups.

For other operating systems, or for more details about Docker on Ubuntu, see the official Docker documentation:

* [Windows](https://docs.docker.com/docker-for-windows/install/)
* [Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
* [Mac](https://docs.docker.com/docker-for-mac/install/)

### Serve the site locally

Either use the script we provide:

```shell
./serve.sh
```

or alternatively, run `mkdocs serve` via Docker:

```shell
$ docker run --rm -it -p 8888:8000 -v ${PWD}:/docs squidfunk/mkdocs-material

INFO    -  Building documentation...
WARNING -  Config value: 'dev_addr'. Warning: The use of the IP address '0.0.0.0' suggests a production environment or the use of a proxy to connect to the MkDocs server.
However, the MkDocs' server is intended for local development purposes only. Please use a third party production-ready server instead.
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.56 seconds
[I 210315 13:12:02 server:335] Serving on http://0.0.0.0:8000
INFO    -  Serving on http://0.0.0.0:8000
[I 210315 13:12:02 handlers:62] Start watching changes
INFO    -  Start watching changes
[I 210315 13:12:02 handlers:64] Start detecting changes
INFO    -  Start detecting changes
[I 210315 13:12:14 handlers:135] Browser Connected: http://127.0.0.1:8888/#bibliography
INFO    -  Browser Connected: http://127.0.0.1:8888/#bibliography
...
```

And open your browser at [http://127.0.0.1:8888](http://127.0.0.1:8888).

### Building the site on disk

It is unlikely that you will need to build the site without also viewing it in a browser.
However, if you do, and you are using Linux, MacOS or another UNIX system you can do this via Docker:

```shell
docker run --rm -it --user $(id -u):$(id -g) -v /etc/passwd:/etc/passwd -v ${PWD}:/docs squidfunk/mkdocs-material build
```

This will build the HTML files and write them to a directory called `site`.
If you wish to use a different directory name, add the switch `-d DIRECTORY_NAME` at the end of that line.

Note that, by default, the Docker container will create files as the `root` user, rather than the user currently logged in.
In the line above, we use the `/etc/passwd` file on disk and the current user name `$(id -n)` and primary group `$(id -g)` to ensure that the files have the right ownership.

If you are not on a UNIX based system, you can remove these options, but you should check the ownership of the files in `site/`:

```shell
docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material build
```

### Custom JavaScript

This repository comes with a script called [`generate_javascript.py`](/generate_javascript.py) which creates some custom JavaScript to display GitLab labels and usernames.
In the Markdown text these are shown as `~Doing` as they would be written in a GitLab issue.

**You will not usually need to run this script in your development environment.**
The only difference you will notice between your development environment and the production site, is that labels and usernames will not be rendered correctly in your development site.

If, for some reason, you do need to debug or maintain this site, please see the [Troubleshooting](#troubleshooting) section.

## Conventions used in this repository

All documents should be in [Markdown format](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/).
Images should go in a `figures` subdirectory.
Diagrams should be created in Markdown, with [Mermaid](https://mermaidjs.github.io/#/).

All Markdown documents should follow the rule of **one sentence per line** (i.e. a line break should follow every full stop).
This makes it *much* easier to view `git diff`s and review merge requests.

There are a variety of ways to [create internal links in mkdocs](https://github.com/mkdocs/mkdocs/blob/master/docs/user-guide/writing-your-docs.md#linking-to-pages).
Keeping internal links clean means that the [LinkChecker](https://github.com/linkchecker/linkchecker) will still give useful feedback when the site is served locally, review apps (deployments from feature branches) will work like the production site, and if we ever move the site to a new domain we will not need to re-write all the internal links.

Please use the following conventions for internal links:

1. Links MUST go to a Markdown file, not an HTML file, i.e. `document.md` and NOT `document.html`,
1. All links should be *relative*, i.e. `../sibling_directory/document.md` and NOT `/sibling_directory/document.md`; and
1. Directory URLs should NOT be used, i.e. `document.md` and NOT `document/`.

### Highlighting PHP code

Note that due to a [limitation in the syntax highlighting libraries](https://github.com/squidfunk/mkdocs-material/issues/506) that we use here, to highlight a block of PHP code you should use three-backticks and `php-inline`, rather than `php`.
This will ensure that your code is highlighted with the correct lexer.

## Testing the website

Because we use [mkdocs](https://www.mkdocs.org/) to generate HTML, we do not check the HTML or styling itself.
However, the pipeline for this repository tests a number of aspects of the Markdown documentation for correctness.

### Ensuring that your Markdown syntax is valid

To make sure that your Markdown is valid, please use [Markdown lint](https://github.com/markdownlint/markdownlint).

To install the lint on Debian-like machines, use the [Rubygems](https://rubygems.org/) package manager:

```shell
sudo apt-get install gem
sudo gem install mdl
```

!!! Warning
    If you are using Ubuntu 18.04, you may need to install the `rubygems` package rather than `gem`.

Because we keep each sentence on a separate line, you will want to suppress spurious `MD013 Line length` reports by configuring `mdl`.
The file `/.mdl.rb` contains styles that deal with `MD013` and other tweaks we want to make to the lint.
To use the style configuration, pass it as a parameter to `mdl` on the command line:

```shell
mdl -s .mdl.rb HOWTO_DOCUMENT.md
```

If you want to run `mdl` from your IDE or editor, you will either need to configure it, or find a plugin, such as [this one for Sublime Text](https://github.com/SublimeLinter/SublimeLinter-mdl).

## Setting up Git hooks

This repository provides two [Git hooks](https://githooks.com/):

* A hook that will run `mdl` each time the developer commits their code, and refuse to perform the commit if the changes do not pass the lint.
* A hook that will build the site (and delete the resulting files) and refuse to push the code if the build fails.

To install the hooks, first ensure that you have `mdl` installed correctly.
Next, run the hook install script:

```shell
./bin/create-hook-symlinks
```

That script creates symbolic links from the files in [`hooks`](/hooks) to the `.git/hooks` directory.

## Troubleshooting

### Checking for broken links

The pipelines also check for broken links, using [LinkChecker](https://linkchecker.github.io/linkchecker/).
You may occasionally need to run the LinkChecker in your development environment, in which case the instructions here should be enough to start you off.
However, we would generally recommend just reading the output of the LinkChecker tool in the pipeline logs on GitLab.

First, build the site.

```shell
docker run --rm -it --user $(id -u):$(id -g) -v /etc/passwd:/etc/passwd -v ${PWD}:/docs squidfunk/mkdocs-material build
```

Then run [LinkChecker](https://linkchecker.github.io/linkchecker/) from Docker:

```shell
docker run --rm -it  -v "$PWD"/site:/mnt ghcr.io/linkchecker/linkchecker index.html
```

You should see something like this:

```shell
$ docker run --rm -it  -v "$PWD"/site:/mnt ghcr.io/linkchecker/linkchecker index.html
WARNING linkcheck.check 2021-03-15 16:11:31,107 MainThread Running as root user; dropping privileges by changing user to nobody.
INFO linkcheck.cmdline 2021-03-15 16:11:31,107 MainThread Checking intern URLs only; use --check-extern to check extern URLs.
LinkChecker 9.4.0              Copyright (C) 2000-2014 Bastian Kleineidam
LinkChecker comes with ABSOLUTELY NO WARRANTY!
This is free software, and you are welcome to redistribute it
under certain conditions. Look at the file `LICENSE' within this
distribution.
Get the newest version at https://linkchecker.github.io/linkchecker/
Write comments and bugs to https://github.com/linkchecker/linkchecker/issues

Start checking at 2021-03-15 16:11:31+000

Statistics:
Downloaded: 367.56KB.
Content types: 50 image, 16 text, 0 video, 0 audio, 43 application, 0 mail and 93 other.
URL lengths: min=8, max=899, avg=79.

That's it. 202 links in 202 URLs checked. 0 warnings found. 0 errors found.
Stopped checking at 2021-03-15 16:11:31+000 (0.72 seconds)

$
```

Or, if you have a broken link:

```shell
$ docker run --rm -it  -v "$PWD"/site:/mnt ghcr.io/linkchecker/linkchecker index.html
WARNING linkcheck.check 2021-03-15 16:10:20,326 MainThread Running as root user; dropping privileges by changing user to nobody.
INFO linkcheck.cmdline 2021-03-15 16:10:20,326 MainThread Checking intern URLs only; use --check-extern to check extern URLs.
LinkChecker 9.4.0              Copyright (C) 2000-2014 Bastian Kleineidam
LinkChecker comes with ABSOLUTELY NO WARRANTY!
This is free software, and you are welcome to redistribute it
under certain conditions. Look at the file `LICENSE' within this
distribution.
Get the newest version at https://linkchecker.github.io/linkchecker/
Write comments and bugs to https://github.com/linkchecker/linkchecker/issues

Start checking at 2021-03-15 16:10:20+000

URL        `assets/images/sdadaFrontPageImage.jpg'
Name       `Traffic3D Front Page Image'
Parent URL file:///mnt/index.html, line 618, col 4
Real URL   file:///mnt/assets/images/sdadaFrontPageImage.jpg
Check time 0.000 seconds
Result     Error: URLError: <urlopen error [Errno 2] No such file or directory: '/mnt/assets/images/sdadaFrontPageImage.jpg'>

Statistics:
Downloaded: 367.56KB.
Content types: 50 image, 16 text, 0 video, 0 audio, 43 application, 0 mail and 94 other.
URL lengths: min=8, max=899, avg=79.

That's it. 203 links in 203 URLs checked. 0 warnings found. 1 error found.
Stopped checking at 2021-03-15 16:10:21+000 (0.67 seconds)

$
```

### Generating custom Javascript

This repository comes with a script called [`generate_javascript.py`](/generate_javascript.py) which creates some custom JavaScript to display GitLab labels.
In the Markdown text these are shown as `~Doing` as they would be written in a GitLab issue.

You will not usually need to run this script, but if you do need to debug or maintain it, you will need:

* A GitLab [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) with API capabilities.
* Some environment variables exported to your shell:
    * `BC_BOT`, `BC_CTO`, `BC_TECH_LEAD` which are all usernames of GitLab staff
    * `GITLAB_INSTANCE` which would normally be `https://gitlab.com`
    * `PERSONAL_ACCESS_TOKEN` which is your [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html)
    * `PROJECT_ID` which you can find on the [front page of this repository on GitLab](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/)

These should be stored in a file called `.env`.
First, copy the template for this file:

```shell
cp .env.template .env
```

then fill in the values for the environment variables.
It would also be a good idea to create a file for the generated Javascript now, so that the Docker container doesn't `chown` it to `root`:

```shell
touch docs/assets/javascript/gitlab.js
```

Next, run the Python script from a Docker container:

```shell
docker run -v ${PWD}:/docs python:3.8-alpine /docs/docker-entrypoint.sh
```

and check that the file has been generated:

```shell
$ ls -l docs/assets/javascript/gitlab.js
-rw-r--r-- 1 snim2 snim2 34265 Mar 19 09:53 docs/assets/javascript/gitlab.js
$
```

## Twitter social card

This site contains a [Twitter social card](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/abouts-cards).
The card will be displayed if a link to the site is posted on a site or application (e.g. Slack) which shows site previews.

The card itself is an image created using the HTML5 code below, which you may want to tweak.
This requires the "white initials" version of the company logo to be available.

The intention is to load this webpage into a browser, and use the screenshot capability of your browser to save the card as a `.png`.
On Chrome, you can use `Ctrl+p screenshot` to search for the relevant command.

Twitter social cards can be validated using the [card validator tool](https://cards-dev.twitter.com/validator).

```html5
<!-- This file can be used to generate social cards. -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
            href="https://fonts.googleapis.com/css2?family=Didact+Gothic&display=swap"
            rel="stylesheet"
        />
        <style language="css">
            :root {
                background: #eee;
                width: max-content;
                 --brand: #795548;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                width: 1200px;
                height: 640px;
                background: var(--brand);
            }

            .card {
                color: white;
                padding-top: 20px;
                position: relative;
            }
            .logo {
                margin-right: 40px;
                margin-top: 40px;
                margin-bottom: auto;
                float: right;
                clear: both;
            }
            h1 {
                margin: 60px 40px 40px 40px;
                font-size: 92px;
            }
            h2 {
                margin: 40px;
                font-size: 32px;
            }
            p {
                 margin: 280px 40px 40px 40px;
                font-size: 28px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="card">
                <img width="auto" height="300px" class="logo" src="initials-white.png" />
                <h2>Beautiful Canoe</h2>
                <h1>Developer<br/>handbook</h1>
                <p>A handbook for everything that happens at Beautiful Canoe.</p>
            </div>
        </div>
    </body>
</html>
```

## Further reading

* [Docker](https://docker.com/)
* [git hooks](https://githooks.com/)
* [LinkChecker](https://linkchecker.github.io/linkchecker/)
* [LinkChecker Docker image](https://github.com/linkchecker/linkchecker/pkgs/container/linkchecker)
* [Markdown lint](https://github.com/markdownlint/markdownlint).
* [Markdown syntax](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
* [Material theme for mkdocs](https://squidfunk.github.io/mkdocs-material/)
* [Mermaid diagrams](https://mermaidjs.github.io/#/)
* [mkdocs](https://www.mkdocs.org/)
* [mkdocs-material](https://squidfunk.github.io/mkdocs-material/)
* [mkdocs-material Docker image](https://hub.docker.com/r/squidfunk/mkdocs-material)
* [Twitter card validator tool](https://cards-dev.twitter.com/validator)
* [Twitter social cards](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/abouts-cards)
