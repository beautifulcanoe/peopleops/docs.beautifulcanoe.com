#!/bin/sh

#
# Only use this script if you need to generate docs/assets/gitlab.js locally.
# Make sure you have filled in the environment variables needed in .env.
#
# Then run this script with:
#    docker run -v ${PWD}:/docs python:3.8-alpine /docs/docker-entrypoint.sh
#

set -e

cd /docs
. .env
pip3 install python-gitlab
python generate_javascript.py
