# Your last week at Beautiful Canoe

We're sorry to see you go, and we hope you've had an excellent time. :canoe:
Before you leave, there are a couple of quick jobs we'd like you to do, just to make life easier for the next developers who will be working on your project.

## Handing over documentation

Some of our projects have technical documentation and meeting notes on [Google Drive](https://drive.google.com/drive/u/0/) or similar.
Before you leave, please make sure that all relevant documentation is owned by someone still in the company, ideally the CTO.
If there is long-lasting documentation in cloud storage, consider moving it to Markdown files in a `docs/` directory in your repository.

Most or all of our projects have [Trello](https://trello.com) boards containing client-facing documentation.
Hopefully all your boards are owned by a team, rather than an individual
That team should include the CTO and @julienbarney as a minimum.

## GitLab

You can ignore GitLab, but if you have information held on any other service, please make sure that you transfer ownership of the information to someone else before you leave.

## Slack

You may or may not want to keep your [Slack](https://beautifulcanoe.slack.com/) account.
This one is up to you, but if you think you might work with us again, you should definitely keep these accounts, to save you having to set them up twice.

## Get a LinkedIn account

If you haven't got one already, it would be a good idea to get an account on [LinkedIn](https://www.linkedin.com/).
This will give recruiters and potential employers a way to get in touch with you, without you having to give out your private contact details.

## Add yourself to the former Canoeists page

Please raise a merge request to remove yourself from the [Team](https://beautifulcanoe.com/about.html#team) section of the website, and add yourself to the [Former Canoeists](https://demo.beautifulcanoe.com/former-canoeists.html) page.
If you have a [LinkedIn](https://www.linkedin.com/) account, make sure you add it to your details.

## Tasks for the leadership team

When you leave, there will be a number of tasks that the leadership team will need to complete:

1. Removing your account from the GitLab groups: this should happen automatically, but it ought to be checked manually.
1. Removing your account from any shared Trello boards, teams, or other cloud accounts.
1. Deactivating your account from the company Slack workspace.
1. Reviewing your MRs to move yourself to the Former Canoeists page on the company website.

## When members of the leadership team leave

When a member of the leadership team leaves, there are some extra tasks to complete:

1. Shared email accounts should be updated.
1. GitLab accounts for members of the leadership team do not expire, and so accounts need to be removed manually.
1. Sometimes we may deactivate the Slack account for a member of the leadership team, but we may also remove them from public channels and keep them on private channels.
