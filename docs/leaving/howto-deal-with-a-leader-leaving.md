# What happens when a leader leaves?

When a member of the leadership team leaves, there are a number of extra tasks that need to be completed.
These mainly deal with documentation and aspects of our automated infrastructure.

## Dealing with documentation

* Move the leader to the *Former Canoeists* page on the company website.
* Replace any references to the leader in this handbook.
* Look through the `README.md`, `CONTRIBUTING.md` and `DEPLOYMENT.md` files in project repositories, and raise merge requests to remove references to the leader.
* Remove the leader from any [Trello](https://trello.com) boards and other client-facing documentation.
* Ensure that the leader does not own any remaining project documents.

## Email infrastructure

* Remove the leader from shared email addresses, such as `alert`.
* Remove the leader's personal email forwarder and replace it with an auto-response which sends people to `hello@`.

## Automated systems

* Some automated repositories have the GitLab usernames stored in environment variables. These should be replaced with the CTO's username.

## Project infrastructure

* Set an expiration date on GitLab group or project memberships. This should be the leaver's last day in the company.

## Back-end infrastructure

* Remove any user accounts the leader has on Beautiful Canoe servers.
