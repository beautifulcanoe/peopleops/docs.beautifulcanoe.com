# Beautiful Canoe Developer Handbook

This website is a handbook for software developers and system administrators at Beautiful Canoe.
Here you can find documentation on our policies, processes and the tools that we use.

Technical documentation that is specific to an _individual project_ should be found in the GitLab repository for that project.
For this sort of documentation, you should start by reading the `CONTRIBUTING.md` file in the root of your project repository.

We do not usually document generic information about languages and tech stacks (for example, tutorials on PHP or Laravel).
Languages and technologies evolve quickly, and you should refer to the official documentation for the specific version the stack you are using.

If you spot anything that's wrong or missing here, please [raise an Issue](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/issues).

## If you are new to Beautiful Canoe

If you are just starting out, please follow [the first day how to](start-here/howto-first-day.md) and then the [the first week how to](start-here/howto-first-week.md).

## If your contract with Beautiful Canoe is about to end

We are sorry to see you go!
If it's your last week there are a [number of things](leaving/howto-last-week.md) we'd like you to do before you leave, just to make life easier for the next student developers who will be working on your project.

## Making changes to this handbook

Please see [CONTRIBUTING.md](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/blob/main/CONTRIBUTING.md) for advice on how to contribute to this repository.
