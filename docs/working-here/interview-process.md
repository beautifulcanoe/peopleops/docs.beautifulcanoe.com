# The Beautiful Canoe interview process

!!! note
    Please note, that if you have applied to work for us before, this process may be different to the one you experienced.
    We are constantly working to improve our processes, so please expect this page to change over time.

## What we are looking for

Our recruitment criteria depend on the vacancies we have available.
Different jobs will require different technical skills (e.g. Java, PHP/Laravel, Unity, etc.) which will suit students at different stages of their studies.
You might be able to demonstrate your technical skills with good module results, but you may also have other evidence of your capabilities, for example previous work for another company, work on open source software, participation in hackathons, and so on.

All of our jobs require good people skills, such as communication and teamwork.
As well as strong technical skills, we will be looking for:

* Your *motivation* -- why you want to work with us and what you hope to get out of your time with us.
* Your *communication skills* with both technical and non-technical colleagues.
* Your ability to work as *part of a team*.
* Your ability to *read code and documentation*.
* Your ability to respond well to *training, feedback and coaching*.

## The process

The interview process at Beautiful Canoe is designed to give you a chance to show yourself at your best, and to help us find applicants who are best suited to working for us.
We recognised that not everyone is well-suited to work with us, especially on our Summer Fellowship scheme, which is intensive.

Like most firms, we have moved to all-remote working, due to the COVID-19 crisis, so our interview process is now online, and we will be looking for candidates that can work confidently in a remote environment.

```mermaid
graph LR
    A["1. Review fa:fa-search"]
    B["2. Survey fa:fa-list-alt"]
    C["3. Video fa:fa-film"]
    D["4. Group task fa:fa-users"]
    E["5. Technical interview fa:fa-sitemap"]
    F["6. Interviews fa:fa-comments"]
    G["7. Offer fa:fa-check"]
    A --> B
    B --> C
    C --> D
    D --> E
    E --> F
    F --> G
```

### 1. Review

When we first receive your application, we will screen it for a variety of criteria, depending on the roles we have available.

### 2. Survey and 3. Video

If we invite you for interview, we will ask you to fill in a short survey and record a short video.
We want our interview process to respect your time, and these tasks will help us to keep the later stages reasonably short.

The survey will contain closed questions, i.e. you will not be asked to write at length, and should only take a few minutes to fill in.
Part of the survey will contain some technical questions which will  help us to prepare for the later stages of the interview process and (if you are successful) to help us prepare for your first week with us.

For the video, we will ask you to record no more than three minutes of yourself answering a number of questions that will be sent to you in your invitation to interview.
We suggest that you upload your footage as an [unlisted video on YouTube](https://support.google.com/youtube/answer/157177).
We will *not* be expecting you to edit the video, nor will be screening you on your camera skills, we are only interested in your answers to the questions.

### 4. Group task (Summer Fellow applicants ONLY)

!!! note
    Note, that this stage of the process is usually only applicable to applicants for our Summer Fellowships.
    However, if we invite you to an interview, we will make it clear whether you will need to take part in a group task.

The first stage of our interviews is a group task.
You **do not need to prepare** for this task and it will not require you to demonstrate any technical skills.

You will join a video call with a small number of applicants, and be asked to perform a task.
The task will be given to you on the video call, and will require you to co-operate with fellow applicants to achieve a goal.

### 5. Technical interview

This stage of our process is an individual interview, via a video call, with one or more Beautiful Canoe employees.
You will be asked to perform a technical task that will be tailored to your prior experience (for example, we will not ask you to write or read any PHP code if you have not told us that you have used PHP before).

The technical interview will *not* ask you exam-style questions or so-called whiteboard coding questions, such as implementing a linked list on a whiteboard.
However, you will be expected to demonstrate general coding skills, including reading and understanding legacy code.
We are not particularly interested in whether you can remember information that can be easily found on Google, for example the syntax of any particular programming language.

**To prepare** for this stage of the interview process, you should review:

* your last practical software development module (if you are a current student on a taught course),
* the last significant piece of software that you have written (if you are not a current student).

### 6. Interview

This stage of our process is an individual interview, via a video call, with two or more Beautiful Canoe employees.
At this stage, we hope to get to know you a little better.

You will be asked about your experience and skills, as well as your career goals.
The interview will not cover technical parts of the job, such as coding skills, software engineering process, and so on.

You **should prepare** by reviewing your CV, cover letter and your video and reflecting on your hopes for your future career.

### 7. Offer

We will make offers to the candidates that we feel are best placed to fulfil the client contracts that we have.
Please note that sending out offer letters may sometimes be delayed, if we are waiting for clients to sign contracts with us.
