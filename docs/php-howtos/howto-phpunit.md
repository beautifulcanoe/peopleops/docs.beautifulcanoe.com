# How to unit test with phpunit

[PHPUnit](https://phpunit.de/) is the industry-standard framework for unit testing PHP code.
All PHP projects should use this package, and MRs which contribute new code should come with unit tests.

## Install PHPUnit

First you'll need to [install composer](https://getcomposer.org/download/)

Next, install PHPUnit with [composer](https://getcomposer.org/download/):

```shell
composer require phpunit/phpunit --dev
```

Notice that the `--dev` switch installs the package for development environments, not deployment environments.

 Now you should have the latest version of PHPUnit:

```shell
$ ./vendor/bin/phpunit --version
PHPUnit 6.5.13 by Sebastian Bergmann and contributors.
```

## Creating a new test

From the root directory of your web application, use `artisan` to create a new test:

```shell
php artisan make:test NameOfTest
```

The new test will be inside `tests/Feature`.

## Conventions

* `$this` has its usual meaning.
* `factory` A factory of is used to create new classes or objects.
* `post` performs an [HTTP post](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html).
* `get` performs an [HTTP get](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html).
* `put` performs an [HTTP put](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html).
* `delete` performs an [HTTP delete](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html).
* `assertStatus` Checks the [HTTP status code](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) in the test.
* `assertSee` Checks what you "see" in the test.

## Example login test

```php
public function testSuccessfulLogin()
{
  $user = factory(\App\User::class)->create([
        'username' => 'Testing',
         'email' => 'testing@testing.com',
         'password' => bcrypt('testpass123')
    ]);

      $response = $this->followingRedirects()
    ->post('/login', ['email' => 'testing@testing.com', 'password' => 'testpass123'])
    ->assertStatus(200)
    ->assertSee($user->username);
}
```

A successful test will look like this:

```shell
$ ./vendor/bin/phpunit
PHPUnit 6.5.13 by Sebastian Bergmann and contributors.

...............................................................  63 / 107 ( 58%)
............................................                    107 / 107 (100%)

Time: 24.65 seconds, Memory: 34.00MB

OK (107 tests, 443 assertions)
```
