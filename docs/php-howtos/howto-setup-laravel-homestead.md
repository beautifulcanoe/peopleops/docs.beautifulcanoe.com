# Homestead Setup instructions

> Laravel Homestead is an official, pre-packaged Vagrant box that provides you a wonderful development environment without requiring you to install PHP, a web server, and any other server software on your local machine. No more worrying about messing up your operating system! Vagrant boxes are completely disposable. If something goes wrong, you can destroy and re-create the box in minutes!

This guide assumes that user will be working with multiple Laravel/PHP projects in the future.
Thus, will aim at setting up Homestead for a machine rather than for a project.

## Installation and Set up

### 1. Set up your machine

Ensure that virtualization support is enabled in your BIOS (for Intel VT and AMD-V based machines).
If you skip this step, then the later steps in this how-to will not work on Ubuntu 64-bit machines.

Note that this is an issue for all the Beautiful Canoe desktop computers.

### 2. Install VirtualBox 5.x and Vagrant

If you are installing Vagrant and VirtualBox on Windows 10 or MacOS X, then follow the instructions on the [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html) websites.

If you are using Ubuntu, or another Debian based system, you can install VirtualBox from the command line:

```shell
sudo apt-get install virtualbox
```

However, the version of Vagrant that is currently packaged with Ubuntu (18.xx) is too old to use with Laravel / Homestead.
Instead of using `apt-get` to install Vagrant, go to the [downloads](https://www.vagrantup.com/downloads.html) page and install the `.deb` file directly.
In Ubuntu, you can either download the `.deb` file, double-click on it and follow the instructions, or from the command line, run:

```shell
sudo dpkg -i vagrant_VERSION.deb
```

Confirm that installation was successful on the command line:

```shell
$  vagrant --version
Vagrant 2.2.9
$ virtualbox --help
Oracle VM VirtualBox Manager 5.2.18_Ubuntu
(C) 2005-2018 Oracle Corporation
All rights reserved.
...
```

### 3. Add Laravel/Homestead installation to Vagrant

```shell
vagrant box add laravel/homestead
```

### 4. Install Homestead

```shell
cd ~
git clone https://github.com/laravel/homestead.git Homestead
cd Homestead
bash init.sh
```

### 5. Configure Homestead

From the Homestead directory, open the `Homestead.yaml` file in your favourite text editor.
This is the main configuration file for Homestead.

#### 5.1 Generate ssh keys

In order to connect to the Vagrant virtual machine, you will use `ssh` (secure shell), which is a standard tool for securely logging into remote or virtual machines.
To do this, you need to generate some public and private keys, for encryption to work correctly.
You will see that `Homestead.yaml` already has these lines which expect the ssh keys to be generated:

```yaml
authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa
```

In a shell, run the `ssh-keygen` tool, similarly to the example below.
When you are asked where to store the key, accept the default location (`~/.ssh/id_rsa`), and when you are asked to enter a passphrase, twice, just press the **Enter** key on your keyboard (i.e. enter a blank passphrase).
For your email address, use the same address you used to create your GitLab account, presumably your Aston account:

```shell
$ ssh-keygen -t rsa -C "USERNAME@aston.ac.uk"
Generating public/private rsa key pair.
Enter file in which to save the key (/home/$USER/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/$USER/.ssh/id_rsa.
Your public key has been saved in /home/$USER/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:...W+c USERNAME@aston.ac.uk
The key's randomart image is:
+---[RSA 2048]----+
...
+----[SHA256]-----+
$
```

#### 5.2 map directories on your hard drive to Vagrant

Next, you will need to *map* the files in your `hello` project (see step 8.) to a directory in your Vagrant virtual machine.

In the `Homestead.yaml` file, set the `folders` path to correspond to the path of your PHP the projects:

```yaml
folders:
    - map: /home/USER/Projects/hello
      to: /home/vagrant/hello
```

where `USER` is your username (if you are not sure, try typing `whoami` on the command line).

Note that `/home/USER/Projects/hello/` is where the files in your `hello` project will live on your hard drive.
`/home/vagrant/hello/` is the directory in Vagrant that we are mapping those files to.

!!! Windows
    If you are using Windows, please note that paths in your `Homestead.yaml` should contain forward-slashes.
    e.g. ` C:/Users/USERNAME/.ssh/id_rsa`, etc.

Next, update the `sites` path to correspond to the location and domain of a project:

```yaml
sites:
    - map: hello.test
      to: /home/vagrant/hello/public
      php: "7.2"
```

This associates the name `hello.test` with the files in `/home/vagrant/hello/public/`.
When you come to look at the project web application in a browser you will look at the location [http://hello.test/index.php](http://hello.test/index.php) and your web server (Apache, that we installed in step 2.) will be serving the file `/home/vagrant/hello/public/index.php`.

### 6. Update the hosts file

When you come to create a new project, you will want to view your web app in a browser, so you will need to know what URL to point your web browser to.
In step 5. above we associated the name `hello.test` with your web application, and we will use this as a URL for the site.

Homestead will associate a local IP address (i.e. one that is only accessible from your machine, not from the local network) with all of your projects.
The IP address is the same for all of your Homestead web applications, even though they will all be mapped to different URLs.

You can find the IP address that you need at the top of the `Homestead.yaml` file.
In the example here the IP is `192.168.56.10`:

```yaml
---
ip: "192.168.56.10"
memory: 2048
cpus: 1
provider: virtualbox
```

So far, you have mapped the files on your disk drive to a virtual folder in Vagrant, and you have associated the web application in that folder with a URL, so that the web server can serve the app.
When you open your browser at the [http://hello.test](http://hello.test) URL, the browser will need to look up the URL, then find its associated IP address.
For a live website, this would happen via [DNS](https://www.cloudflare.com/learning/ddos/glossary/domain-name-system-dns/), but since this is a local IP address that cannot be seen outside of your computer, you need to tell your OS to associate the URL with the IP address.

To do this, open the system `hosts` file (e.g. `/etc/hosts` in Ubuntu and MacOS X, `C:/Windows/system32/drivers/etc` in Windows 10) using your text editor of choice and add an IP address and a domain name of the project.
For example:

```plaintext
127.0.0.1       localhost
192.168.56.10   hello.test
```

!!! Important
    In some versions of this document, we recommended using `192.168.10.10` for Homestead.
    However, due to [this issue](https://github.com/laravel/homestead/issues/1717) that particular IP address may cause timeouts for users with later versions of VirtualBox.
    If you have an old `Homestead.yaml` file, it would be wise to update it, along with your `/etc/hosts` file.

### 7. Vagrant up

In the terminal, navigate to the Homestead folder and run `vagrant up` to start Vagrant running:

```shell
cd ~/Homestead
vagrant up
```

### 7.1 Change default PHP version

Currently all our production and staging servers run PHP 7.2, but the default version of PHP on Homestead is 7.3.
This will cause problems whenever you update `composer` dependencies in one version of PHP and then try to deploy them to a server running a different version of PHP.
To avoid these issues, change the default version of PHP in your Homestead.
First, ssh to vagrant:

```shell
$ vagrant ssh
Welcome to Ubuntu 18.04.1 LTS (GNU/Linux 4.15.0-45-generic x86_64)

 _                               _                 _
| |                             | |               | |
| |__   ___  _ __ ___   ___  ___| |_ ___  __ _  __| |
| '_ \ / _ \| '_ ` _ \ / _ \/ __| __/ _ \/ _` |/ _` |
| | | | (_) | | | | | |  __/\__ \ ||  __/ (_| | (_| |
|_| |_|\___/|_| |_| |_|\___||___/\__\___|\__,_|\__,_|

* Homestead v10.8.1
* Settler v9.5.1 (Virtualbox, Parallels, Hyper-V, VMware)


 * Canonical Livepatch is available for installation.
   - Reduce system reboots and improve kernel security. Activate at:
     https://ubuntu.com/livepatch

353 packages can be updated.
166 updates are security updates.


Last login: Fri Jun 12 11:05:23 2020 from 10.0.2.2
vagrant@homestead:~$
```

and then use `update-alternatives` to change the default versions of all PHP binaries:

```shell
sudo update-alternatives --set php /usr/bin/php7.2
sudo update-alternatives --set phar /usr/bin/phar7.2
sudo update-alternatives --set phar.phar /usr/bin/phar.phar7.2
sudo update-alternatives --set phpize /usr/bin/phpize7.2
sudo update-alternatives --set php-config /usr/bin/php-config7.2
```

### 7.2 Changing Homestead.yaml

If you ever need to make a change to your configuration, you will need to reload Vagrant:

```shell
vagrant reload --provision
```

### 8. Test the set up

In your home directory (`cd`) create a `Projects` folder and in there create the `hello` and `public` folders:

```shell
mkdir -p $HOME/Projects/hello/public
```

In a text editor, or on the command line, create a simple *Hello world!* PHP file inside the `hello/public` directory:

```shell
$ cat >Projects/hello/public/index.php <<EOF
<?php
  echo "Hello, world!";
EOF
```

type **Ctrl+d** to close `cat`.

Lastly, using a web browser, visit [http://hello.test](http://hello.test).
If the browser displays the message *Hello, world!* (without the surrounding `<?php` etc.), then everything worked!

## Creating a new Laravel project

### 1. Create a sites or projects folder

If you haven't followed the instructions above, to install Homestead, first make sure that you have a folder called `Projects`.
This can be anywhere, and it will contain all the Laravel web applications you will create.

### 2. Make Laravel commands available

Before you go any further, you need to make Laravel commands available.
At the moment, if you try to tab-complete `larave` you will notice that there isn't a command called `laravel` available on your `$PATH`.
To fix this, you can use the `composer` package manager:

```shell
composer global require laravel/installer
```

### 3. Create the web application project within the projects folder

At the command line, go to the `Projects` folder and run `laravel new`:

```shell
cd ~/Projects
laravel new hello-laravel
```

This will create a new project and website called `hello-laravel`.

### 4. Update the Homestead.yaml file

You may already have defined `map-to` structure within the `Homestead.yaml` file, such as this example above:

```yaml
folders:
    - map: /home/USER/Projects/hello
      to: /home/vagrant/hello

...

sites:
    - map: hello.test
      to: /home/vagrant/hello/public
      php: "7.2"
```

When adding another website, add another `map` section with a different website name, and replace the `hello` part of the `to` line with the name of the newly created projects folder.
Your updated `Homestead.yaml` should look similar to this:

```yaml
folders:
    - map: /home/USER/Projects/hello
      to: /home/vagrant/hello
    - map: /home/USER/Projects/hello-laravel
      to: /home/vagrant/hello-laravel

...

sites:
    - map: hello.test
      to: /home/vagrant/hello/public
      php: "7.2"
    - map: hello-laravel.test
      to: /home/vagrant/hello-laravel/public
      php: "7.2"
```

### 5. Reload Vagrant

From the `Homestead` directory, reload Vagrant:

```shell
cd ~/Homestead
vagrant reload --provision
```

You will need to run this `reload` command every time you update the `Homestead.yaml` file.

### 6. Update the hosts file

Open the system `hosts` file (e.g. `/etc/hosts` in Ubuntu and MacOs, `C:/Windows/system32/drive/etc` in Windows 10) using your text editor of choice and add an IP address and a domain name of the project.
For example:

```plaintext
127.0.0.1       localhost
192.168.56.10   hello.test
192.168.56.10   hello-laravel.test
```

You can find the IP address that you need at the top of the `Homestead.yaml` file:

```yaml
---
ip: "192.168.56.10"
memory: 2048
cpus: 1
provider: virtualbox
```

## Troubleshooting

### Ubuntu

If you have Ubuntu installed with UEFI Secure Boot enabled, you may find that VirtualBox will not run.
In this case, you need to *sign* the kernel modules that VirtualBox uses.
Follow the instructions [on this blog post](https://stegard.net/2016/10/virtualbox-secure-boot-ubuntu-fail/).

### MacOS

If you are running Mojave 10.14.5 you may encounter a bug which prevents VirtualBox from running.
To fix this, follow these steps which come from [this forum post](https://forums.virtualbox.org/viewtopic.php?f=8&t=89769#p439951):

1. Use the following command to get the Team ID: `codesign -dv --verbose=4 /Applications/VirtualBox.app` which should give you a result like `TeamIdentifier=VB5E2TV963`.
1. Turn on your Mac, then immediately press and hold `Command-R` to start up  MacOS Recovery.
1. Select `Disk Utility` from the `Utilities` window, then click `Continue`. From the `Disk Utility` sidebar, select the volume that you're using, then choose *File > Mount* from the menu bar.
1. Enter your administrator password.
1. Quit `Disk Utility`.
1. Choose `Terminal` from the `Utilities` menu in the menu bar.
1. Type in: `spctl kext-consent add VB5E2TV963`
1. Choose `Apple () menu > Restart`.

### Virtualbox within Windows

If you installing Laravel/Homestead on Ubuntu running on Virtualbox running on Windows, you may need to take some extra steps.

In Virtualbox, make sure that you have `Acceleration` turned on in your CPU settings.
You may have a setting called `VT-x` or similar.
The documentation for Virtualbox says:

> Enable Nested VT-x/AMD-V: Enables nested virtualization, with passthrough of hardware virtualization functions to the guest VM.

If that option is greyed out for you, please follow the instructions in [this StackOverflow discussion](https://stackoverflow.com/questions/54251855/virtualbox-enable-nested-vtx-amd-v-greyed-out).

If possible, you should also make sure that you give Ubuntu more than one CPU.

In your `Homestead.yaml` file, make sure that you have only 1 CPU listed, e.g:

```yaml
---
ip: "192.168.56.10"
memory: 2048
cpus: 1
provider: virtualbox
```

If you get an error such as:

```shell
.../handle_forwarded_port_collisions.rb:204:in `initialize': Permission denied @ rb_sysopen - ~/.vagrant.d/data/fp-leases/2200 (Errno::EACCES)
```

it is likely that some of your files in `~/.vagrant.d/` are owned by `root`.
Change this with:

```shell
sudo chown -R $USER.`id -gn $USER` ~/.vagrant.d
```

### If all else fails

If you still cannot get `vagrant up` to work, first generate some debug output:

```shell
VAGRANT_LOG=DEBUG vagrant up
```

look for clues to what happen, and ask for help our our [Slack workspace](https://beautifulcanoe.slack.com/#/).

## Further reading

* [Laravel Homestead v5.2](https://laravel.com/docs/5.2/homestead)
* How to set-up [Laravel Homestead in Windows](https://www.youtube.com/watch?v=llu2982UL60&t=415s)
* [Laravel tutorial: Step by Step Guide on Building your First Web Application](https://laravel-news.com/your-first-laravel-application)
* [Laravel MVC tutorial: Step by Step Guide on Laravel MVC](https://www.youtube.com/watch?v=Xfgzdi4p7bo)
