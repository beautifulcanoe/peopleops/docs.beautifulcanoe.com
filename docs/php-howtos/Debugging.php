<?php
namespace App;

class Debugging
{
    // This will keep track of all the full traces
    public static $fullTraces = array();

    
    public static function phpObjectToChrome($object, $objectName)
    {    
   
        // get the full trace for this function call
        self::$fullTraces[] = debug_backtrace();
        // Convert the full traces to json before returning them in the js
        $jsonFullTraces = json_encode(self::$fullTraces);


        // Get the trace code for this call so that it can be refered to
        $callFullTraceCode = (string)(Count(self::$fullTraces) -1);
        
        // Convert php object to an array
        $objectArray = static::object_to_array($object);

        // convert the php array to json so that js can decode it and convert it into an array
        $objectJson = json_encode($objectArray);
        
        // Get the short infor for this call
        $shortCallInfo = static::getShortCallInfo($callFullTraceCode);
        $shortCallInfoClass = $shortCallInfo['class'];
        $shortCallInfoFunction =$shortCallInfo['function'];
        $shortCallInfoFile =$shortCallInfo['file'];
        $shortCallInfoLine =$shortCallInfo['line'];
  

        echo "<script>

        console.log('Class: '+'$shortCallInfoClass' + ' -- ' + '$shortCallInfoFunction' + ' -- ' + '$shortCallInfoLine' );
        console.log('File: '+'$shortCallInfoFile');
        console.log('Code: ' + '$callFullTraceCode ')
        console.log('ObjectName: '+'$objectName')

        console.log($objectJson)
        
        var fullTraces = [];
        fullTraces = $jsonFullTraces;

        
        function fullTrace(id)
        {
            console.log('Full trace : ')
            console.log(fullTraces[id]); 
        }

        console.log(' ----------------------------------------------------------- ')

        </script>";

    }
    private static function object_to_array($data)
    {
        if (is_array($data) || is_object($data)) {
            $result = array();
     /*console.log('Full trace : ')
         console.log($fullTraceInfoJson); */
            foreach ($data as $key => $value) {
                $result[$key] = static::object_to_array($value);
            }
           
            return $result;
        }
    
        return $data;
    }
    private static function getShortCallInfo($callFullTraceCode)
    {
        $shortInfo= array();
        // get class
        if (array_key_exists('class', self::$fullTraces[$callFullTraceCode][1])) {
            $shortInfo["class"] = self::$fullTraces[$callFullTraceCode][1]['class'];

            // find file using class
            $classInfo = new \ReflectionClass($shortInfo["class"]);
            $shortInfo['file'] = $classInfo->getFileName();
        } else {
            $shortInfo['class'] = "??";
            $shortInfo['file'] = "??";
        }

        if (array_key_exists('function', self::$fullTraces[$callFullTraceCode][1])) {
            $shortInfo['function'] =self::$fullTraces[$callFullTraceCode][1]['function'];
        } else {
            $shortInfo['function'] = "??";
        }

        if (array_key_exists('line', self::$fullTraces[$callFullTraceCode][1])) {
            $shortInfo['line'] =self::$fullTraces[$callFullTraceCode][1]['line'];
        } else {
            $shortInfo['line'] = "";
        }

        return $shortInfo;
    }
}
