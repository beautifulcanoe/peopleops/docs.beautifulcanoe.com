# How to format code with PHPStorm

In phpStorm go to file-settings or press `ctrl+alt+s`.
In **Settings** go to **Code style – PHP**.
Then click on **Set from...**:

![image](./figures/psr1_psr2_phpstorm_settings.png)

Then choose **Predefined Style** from the drop down menu, followed by **PSR1/PSR2**.

You can then click on **Apply** in the bottom right corner.

This will return you to your code, but you may notice there has been no change.
You now need to press **ctrl-Alt-L** and you will see any changes than need to be made to conform your code to PSR1/PSR2 happen automatically.

Before:

![image](./figures/psr1_psr2_before.png)

After:

![image](./figures/psr1_psr2_after.png)
