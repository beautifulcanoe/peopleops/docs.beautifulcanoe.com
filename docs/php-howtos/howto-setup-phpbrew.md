# About PHPBrew

PHPBrew is a tool to help you run multiple versions of PHP on the same operating system.
For example, you might have PHP 7 installed by default:

```shell
$ php --version
PHP 7.2.10-0ubuntu1 (cli) (built: Sep 13 2018 13:38:55) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.10-0ubuntu1, Copyright (c) 1999-2018, by Zend Technologies
```

but you might need to switch to PHP 5 to work on a legacy project.

## Installing PHPBrew

First, you have need to download PHPBrew from GitHub:

```shell
mkdir ~/bin
cd ~/bin
curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew
chmod +x phpbrew
```

Log out and back into your system, then initialize PHPBrew:

```shell
phpbrew init
```

Add this line at the end of your `~/.bashrc` file:

```shell
[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc
```

Next, you need to update your Bash shell to run the new script as if you had just logged in:

```shell
source ~/.phpbrew/bashrc
```

Install the build dependencies for PHP5.
In Ubuntu 17.10, you will need to run these commands:

```shell
sudo apt-get install -y php-pear autoconf automake curl libcurl3-openssl-dev build-essential libxslt1-dev re2c libxml2 libxml2-dev bison libbz2-dev libreadline-dev
sudo apt-get install -y libfreetype6 libfreetype6-dev libpng12-0 libpng12-dev libjpeg-dev libjpeg8-dev libjpeg8  libgd-dev libgd3 libxpm4 libltdl7 libltdl-dev
sudo apt-get install -y libssl-dev openssl
sudo apt-get install -y libgettextpo-dev
sudo apt-get install -y libicu-dev libmhash-dev libmcrypt-dev
sudo apt-get install -y libcurl4-openssl-dev
cd /usr/include
sudo ln -s x86_64-linux-gnu/curl
```

Now you can build and set up PHP 5.6.35 with the default + DB variants.
You may want to add the `+fpm` variant if you are setting up PHPBrew in a server: `php-fpm` allows PHP to run on a different user than the webserver, and it can be used to run multiple versions of PHP on the same Apache setup.
If it fails, try adding `-d` before `install` and run it again to see more information on the issue (you might be missing build dependencies, see prerequisites at top):

```shell
phpbrew install --downloader=wget 5.6.35 +default+dbs+fpm
```

When it's done, you will have your own build of PHP 5.6.35.

## Using a non-system PHP with Apache

If you are running Apache, for example, if you are using [Homestead](howto-setup-laravel-homestead.md) as your development environment, then you will need to make some adjustments to use a non-system version of PHP.
The `+fpm` switch we used when installing PHP 5 above, installed the Apache [Fast Process Manager](https://cwiki.apache.org/confluence/display/HTTPD/PHP-FPM), which runs PHP from within the Apache web server.

To use non-system versions of PHP via Apache, please follow the [php-fpm howto](../infrastructure-howtos/howto-use-php_fpm.md).

## Building with XDebug support

If you need it, you might want to install the XDebug extension.
Switch to the new PHP and install the relevant extension:

```shell
phpbrew use 5.6.35
phpbrew ext install --downloader=wget php_stream xdebug 2.5.5
```

## Building with other extensions

Similarly, you may need to install other extensions for your particular project.
Do this in the same way that `xdebug` support is installed (see above).
For example, to enable support for the `mongo` database:

```shell
phpbrew use 5.6.35
phpbrew ext install --downloader=wget php_stream mongo
```

## IDE support

Be sure to tell your IDE about the version of PHP you are using.
For example, the version in this running example will be installed in `~/.phpbrew/php/php-5.6.35`.
If you have `xdebug` support installed, make sure you tell your IDE that it is available, so you can use your IDE for step-by-step debugging.

## Building with LDAP support

If you need LDAP, you will need some extra packages installed.
In Ubuntu, try:

```shell
sudo apt-get install libldb-dev libldap2-dev
```

and then build PHP with:

```shell
phpbrew install --downloader=wget 5.6.35 +default+dbs+ldap+fpm
```

## Fixing common problems

### Cannot find `libpq-fe.h`

First you may need to install Postgresql before you install PHPBrew otherwise you may come across an error later in the installation process.

```shell
configure: error: Cannot find libpq-fe.h. Please specify correct PostgreSQL installation path
```

In Ubuntu, either try:

```shell
sudo apt-get install postgresql-server-dev-9.5
```

Or:

```shell
sudo apt-get install libpq-dev
```

### openssl issues on Ubuntu

If you see an error like this:

```shell
Error: Make failed:
The last 5 lines in the log file:
~/.phpbrew/build/php-5.6.35/ext/openssl/openssl.c: In function ‘zif_openssl_decrypt’:

~/.phpbrew/build/php-5.6.35/ext/openssl/openssl.c:5316:17: error: storage size of ‘cipher_ctx’ isn’t known

  EVP_CIPHER_CTX cipher_ctx;

                 ^~~~~~~~~~

make: *** [Makefile:534: ext/openssl/openssl.lo] Error 1
```

then your version of OpenSSL may be too new for the version of PHP that you are trying to install.
Edit the file `/etc/apt/sources.list` and add these lines to the end of the file:

```shell
deb http://httpredir.debian.org/debian jessie main
deb-src http://httpredir.debian.org/debian jessie main

deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main
```

Then update apt:

```shell
sudo apt-get install debian-keyring debian-archive-keyring
sudo apt-get update
```

and install version 1.0 of OpenSSL:

```shell
sudo apt-get install openssl1.0
sudo apt-get install libssl1.0-dev
sudo apt-get install libcurl4-gnutls-dev
```

Note that installing `libssl1.0-dev` will un-install `libcurl`, so in the commands above we have installed `libcurl4-gnutls-dev` instead, which is compatible with the version of OpenSSL that we need.

Now install PHP again:

```shell
phpbrew install --downloader=wget 5.6.35 +default+dbs+fpm
```
