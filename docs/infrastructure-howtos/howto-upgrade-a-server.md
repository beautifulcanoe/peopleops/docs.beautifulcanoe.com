# How to upgrade a server

## Preparation

Before an upgrade can be performed, the current OS needs to be brought up to date, and some dependencies need to be installed.

### Bring all packages up to date

Upgrade all packages on the server:

```shell
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get autoremove
```

### Install dependencies

Next, ensure that the upgrade services themselves are installed, and [screen](https://www.gnu.org/software/screen/):

```shell
sudo apt-get install update-manager-core screen
```

### Configure apt and dpkg

In order to ensure that the the upgrade is non-interactive, we need to configure `apt` and `dpkg` to always keep the old configuration of any package it updates.
Normally, every time that `dpkg` installs a new configuration file that has been modified, it will stop the upgrade and ask you whether you want to keep your modified configuration or replace it with one from the package maintainer.
For upgrading a headless server, we want to ensure that the upgrade does not depend on any human interaction, in case the `ssh` connection with the server drops during the upgrade process.
We need to make use of the following `dpkg` settings:

* `--force-confold` which means *do not modify the current configuration file*, the new version of the file will be written to disk with a `.dpkg-dist` file extension. With this option alone, even configuration files that you have not modified are left untouched, but if you combine it with `--force-confdef` then `dpkg` will overwrite configuration files that have not been modified.
* `--force-confdef` which means *ask dpkg to decide alone when it can safely replace an unmodified configuration file*, and prompt otherwise. This is default behaviour.

See the [dpkg manpage](https://linux.die.net/man/1/dpkg) for more details.

To make these settings permanent, open `/etc/apt/apt.conf.d/local` in a text editor, and add the following text (if it isn't already there):

```shell
Dpkg::Options {
"--force-confdef";
"--force-confold";
}
```

### Reboot

Just in case the Linux kernel has been replaced since the last reboot, reboot now:

```shell
sudo reboot
```

## Upgrade

Run the upgrade task itself:

```shell
sudo do-release-upgrade -f DistUpgradeViewNonInteractive
```

Once the upgrade has finished, check that you have a new Ubuntu version:

```shell
lsb_release -a
```

and ensure that the following packages are installed:

```shell
sudo apt-get install sssd-dbus libsss-simpleifp0
```

!!! Warning
    If you miss this step, the VM may not come back up after a reboot.

## Reboot

Once the upgrade has finished *and* you have run `sudo apt-get install sssd-dbus libsss-simpleifp0` reboot with:

```shell
sudo reboot
```

## Documentation

Once you have completed the upgrade and checked that the machine is sending date to [our Nagios server](howto-set-up-nrpe-monitoring.md) make sure you update the [Server Map](https://gitlab.com/beautifulcanoe/leadership/management-wiki/-/wikis/Server-map).
You will need to check that all the columns for the server are up to date.

## Troubleshooting

Since you are upgrading over `ssh`, `do-release-upgrade` will automatically start a [screen](https://www.gnu.org/software/screen/) session, in case your `ssh` connection drops.
Because you are running the upgrade task non-interactively, you will not see the notice that is normally printed to the terminal, which is:

```shell
... an additional SSH daemon will be started at port
'1022'.
```

If your connection does drop out, try connecting again (using the alternative port, if necessary) and run `screen -r` to re-attached to the session.

## Further reading

* [dpkg manfiles](https://linux.die.net/man/1/dpkg)
