# How to set up Unity cache server

Unity builds can be very slow, if they try to render assets.
Running a [Unity cache server](https://github.com/Unity-Technologies/unity-cache-server) can improve pipelines times by caching rendered assets.

## Install nodejs v10

In Ubuntu 18.04, the packaged version of NodeJS is too old for [Unity cache server](https://github.com/Unity-Technologies/unity-cache-server).
So, a PPA needs to be used:

```shell
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## Install Unity cache server with `npm`

```shell
sudo npm install unity-cache-server -g
```

Note that `-g` installs the Node module system-wide.

## Create a new user

Create a new user, with no shell and minimal privileges, to run the server:

```shell
sudo adduser --system unitycache
sudo -u unitycache bash
```

Note that we need to start Bash on the command-line as `unitycache` does not have a default shell.

## Create configuration files

```shell
sudo -u unitycache bash
cd /home/unitycache
mkdir {config,cache}
unity-cache-server --save-config config/default.yml
```

## Start the server as a daemon

The cache server should be run as a background process, by the `unitycache` user.
Create shell a script in `/etc/init.d/unity-cache` to make this happen:

```shell
#!/bin/sh

### BEGIN INIT INFO
# Provides: unity_cache_server
# Required-Start: $network $remote_fs
# Required-Stop: $network $remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Unity Cache Server.
# Description: Unity Cache Server provides fast importing
# of assets that have already been processed by any member of the team.
### END INIT INFO

if [ -f /etc/init.d/functions ] ; then
# Source function library.
. /etc/init.d/functions
else
echo_success () { echo -n " [ OK ] " ; }
echo_failure () { echo -n " [FAILED] " ; }
fi

if [ -f /etc/sysconfig/network ] ; then
# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ X\${NETWORKING} = Xno ] exit 0

fi

##
# UnityCacheServer startup script
##

################################################################################
## EDIT FROM HERE
################################################################################

# Installation prefix
CSUSER="unitycache"

################################################################################
## STOP EDITING HERE
################################################################################

# The path that is to be used for the script
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

CACHE_HOME="/home/unitycache"
DAEMON="nohup unity-cache-server --NODE_CONFIG_DIR=${CACHE_HOME}/config -P ${CACHE_HOME}/cache &"
RETVAL=0

start () {
echo -n "Starting Unity Cache Server: "
cd $CACHE_DIR
su -s /bin/bash -l $CSUSER -c "${DAEMON}"
RETVAL=$?
if [ $RETVAL -eq 0 ]; then
echo_success
echo
else
echo_failure
echo
exit 1
fi
}

stop () {
echo -n "Stopping Unity Cache Server: "
su -s /bin/bash -l $CSUSER -c "kill $(ps ax | grep unity-cache-server | grep -v grep | awk '{print $1}')"
RETVAL=$?
if [ $RETVAL -eq 0 ]; then
echo_success
else
echo_failure
fi
echo
}

restart () {
stop
start
}

dostatus() {
ps ax | grep unity-cache-server | grep -v grep | awk '{print $1}'
RETVAL=$?
}


# See how we were called.
case "$1" in
start)
start
;;
stop)
stop
;;
status)
dostatus
;;
restart)
restart
;;
*)
echo "Usage: $0 {start|stop|status|restart}"
exit 1
esac

exit $RETVAL
```

Next make the daemon executable, and start the server with:

```shell
sudo chmod +x /etc/init.d/unity-cache
sudo /etc/init.d/unity-cache start
```

and check its status with:

```shell
sudo /etc/init.d/unity-cache status
```

## Create a cron job to clean the cache

The cache should be cleaned regularly (say, hourly).
Run `sudo crontab -u unitycache -e` and add an entry for the command `/usr/bin/unity-cache-server-cleanup -P /home/unitycache/cache/ -d`.

An hourly cron job should look like this:

```shell
$ crontab -l
# m h  dom mon dow   command
0    *    *    *    *    /usr/bin/unity-cache-server-cleanup -P /home/unitycache/cache/ -d
```

## Open firewall port

If you have Docker images that need to connect to the cache server, you will also need to open the relevant port in the firewall:

```shell
sudo ufw allow in on docker0 from 172.17.0.0/16 to 172.17.0.0/16
```

where `172.17.0.1:8126` is the address of the host machine.
THis should be checked with `ifconfig` before changing the firewall rules.

## Telling Unity3d to use the cache server

Once the server is running, invoke Unity with the command-line option: `-CacheServerIPAddress host:port`.
By default, `host:port` will be `127.0.0.1:8126` but you are likely to need `172.17.0.1:8126` if Unity is running within Docker.

## Check that the cache server is working correctly

Once you have completed the above steps, it is important to check that the cache server is being used by Unity.
Run a relevant GitLab pipeline, and run the following on the server:

```shell
watch -n 1 ls -lh /home/unitycache/cache
```

Once Unity is running on the server, you should see new files and directories appearing in the `cache` directory.
