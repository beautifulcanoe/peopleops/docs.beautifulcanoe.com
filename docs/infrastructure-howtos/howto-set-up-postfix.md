# How to set up postfix

[Postfix](http://www.postfix.org/) is a UNIX mail server.
On Beautiful Canoe servers we use it to send security updates and similar to `tech@beautifulcanoe.com`.

## Prerequisites

On Ubuntu install dependencies:

```shell
sudo apt-get install postfix mailutils libsasl2-modules
```

## Configuring Postfix

### Configuring Postfix within the VPN

Edit `/etc/postfix/main.cf` in your favourite text editor and search for `[relayhost]`.
This should be set to:

```yaml
relayhost = [smtp.aston.ac.uk]
```

### Configuring Postfix on cloud servers

On external servers we use [SendinBlue](https://www.sendinblue.com/) transactional email service.
Go to the [account page](https://account.sendinblue.com/advanced/api/) and create a new SMTP key for the server.
Make sure that it is clear from the name of the key which server will use it, and note that once you have saved the key it will **not** be visible again.

Create a new file `/etc/postfix/sasl_passwd` with the following contents:

```shell
[smtp-relay.sendinblue.com]:587 sarah@beautifulcanoe.com:SMTPKEY
```

where `SMTPKEY` is the created previously.
Next, change its permissions to be only read/write-able by the owner:

```shell
sudo chmod 600 /etc/postfix/sasl_passwd
```

and run `postmap` to create the password database:

```shell
sudo postmap /etc/postfix/sasl_passwd
```

Next, open the `/etc/postfix/main.cf` in a text editor.
There may be a large number of default options in that file, and most can be left alone (such as the `hostname`, `mydestination`, etc.) but these options need to be set:

```shell
smtp_sasl_security_options = noanonymous
smtp_tls_security_level = may
relayhost = [smtp-relay.sendinblue.com]:587
smtp_sasl_auth_enable = yes
smtp_use_tls = yes
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
header_size_limit = 4096000
```

### Verifying domains and senders

If it hasn't been done already, go to the [SendinBlue senders page](https://account.sendinblue.com/senders) and add any domains or senders that you have used.
Follow the on-screen instructions to verify the domain or sender (you may also need access to our 1-2-3-reg account).

## Restart postfix

Restart postfix to apply the configuration changes and ensure that it will start on boot:

```shell
sudo systemctl enable postfix
sudo systemctl restart postfix
```

## Open SMTP in the firewall

Ensure that SMTP services can get through the firewall:

```shell
sudo ufw allow smtp
```

## Test postfix

To test postfix, send an email from the command line, which requires the `mailutils` package:

```shell
mail -s Test USER@DOMAIN <<<"Test"
```

and check that the email arrives in your usual mailbox.

## Set up the aliases database

If you now look in `/var/log/syslog` you are likely to see lines like this:

```shell
... postfix/local[6576]: warning: hash:/etc/aliases is unavailable. open database /etc/aliases.db: No such file or directory
... postfix/local[6576]: warning: hash:/etc/aliases: lookup of 'root' failed
```

At this stage, the file `/etc/aliases` has been created by postfix, but the database `/etc/aliases.db` has not.
To fix this, run `postalias`:

```shell
sudo postalias /etc/aliases
```

## Configure cron jobs to send emails

`cron` and `anacron` are UNIX utilities to run commands at specified intervals, usually hourly, daily, weekly or monthly.
These utilities are usually used to automate server management tasks such as performing backups and rotating logs.

To configure `cron` to send email to a mailbox that is likely to be read (rather than `root@localhost`), edit `/etc/crontab` (and `/etc/anacrontab`, if there is one) and change the line that starts with `MAILTO`:

```yaml
MAILTO=tech@beautifulcanoe.com
```
