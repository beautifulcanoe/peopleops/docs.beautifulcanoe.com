# How to update ssl certificates

Beautiful Canoe owns a small number of domains, which all hold SSL certificates provided by [Let's Encrypt](https://letsencrypt.org).

If one of these needs to be updated, an email will be sent to `tech@beautifulcanoe.com`.
In order to update a certificate, you need `ssh` access to the relevant server, your account on the server should be capable of running `sudo` and you should normally be on the `tech@beautifulcanoe.com` mailing list.
If you are not on the mailing list, or you do not have privileges on the relevant server, and you believe you should have, please ask the CTO for advice.

## Let's Encrypt cron jobs

By default, servers should have a `cron` job set up to auto-renew certificates on a daily basis.
The [EFF site](https://certbot.eff.org/) says:

> if you're setting up a cron or systemd job, we recommend running it twice per day (it won't do anything until your certificates are due for renewal or revoked, but running it regularly would give your site a chance of staying online in case a Let's Encrypt-initiated revocation happened for some reason). Please select a random minute within the hour for your renewal tasks.

A script such as the one below should be found in `/etc/cron.daily`:

```shell
#!/bin/sh

#
# File /etc/cron.daily/update-ssl-certs
#

set -e

/opt/eff.org/certbot/venv/bin/certbot renew --quiet
```

Make sure to run `chmod +x` on the file, and ensure that it is owned by `root`.

Also, check that `/etc/crontab` or `/etc/anacrontab` contains the line:

```yaml
MAILTO=tech@beautifulcanoe.com
```

so that debug output is read by an administrator and does not just sit on the server.

To test the script, add `--dry-run` to the line that runs `certbot` and run from the command line as root.

For more ideas on how to test cron jobs, [this SO question](https://stackoverflow.com/questions/4984725/how-to-test-a-weekly-cron-job) is helpful.

## Updating a certificate manually

Hopefully, all Beautiful Canoe servers have SSL update `cron` jobs set up, so you should not need to update certificates manually.
However, if you have to, these are the steps to follow:

`ssh` in to the server, and look for the `letsencrypt` install.
This will usually be in `/opt/letsencrypt` or `/opt/eff.org/`, or occasionally the `certbot` binary will be in `$PATH`:

```shell
cd /opt/letsencrypt
```

Next, run the `certbot` tool to update SSL registration:

```shell
sudo ./certbot renew
```

## More information

For more information please see the [Let's Encrypt documentation](https://letsencrypt.org/docs/expiration-emails/).
