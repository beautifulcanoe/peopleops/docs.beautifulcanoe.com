# How to sunset a project

When a project has finished, and any relevant assets have been sent back to the client, the technical infrastructure for the project needs to be dismantled.
As a general rule, we prefer to leave the the infrastructure set up in such a way that it can be re-activated if the client comes back to us.

## Disable deployments

Disable the Apache configurations for the site by running `a2dissite SITE`.
Move the relevant Apache configuration to `/etc/apache2/sites-available/mysite.conf-disabled`, then reload Apache:

```shell
sudo service apache2 reload
```

Remember to do this for production, staging and any other relevant deployments.
Remove any public deployments from the company account on [uptimerobot](https://uptimerobot.com/).
Remove any webhooks from our [Slack app](https://api.slack.com/apps/AH31R0QH5).
Revoke any SSL certificates that are associated with the project, and remove scripts for the [automatic renewal](howto-update-ssl-certs.md) of SSL certificates.
You should also raise an issue and an MR to remove the site from our SSL validation script in the [cron repository](https://gitlab.com/beautifulcanoe/devops/cron).

## Stop GitLab runners

Remove the runners from GitLab, by going to **Settings -> CI/CD -> Runners** and clicking on the **Remove runner** button for each one.
Move the gitlab runner config on the server from `/srv/http/SITE/gitlab-runner/config.toml` to `/srv/http/SITE/gitlab-runner/config.toml.old`.
In some older servers the configuration might be in `/home/gitlab-runner`.

## Remove SSL certificates

Remove the SSL certificates obtained for the site(s) by running:

```shell
certbot delete
```

and selecting the relevant domains.
In some older servers the `certbot` script might be found in `/opt/eff.org/...`.

## Update the SSL validator

Our [SSL validator](https://gitlab.com/beautifulcanoe/devops/cron/) runs as a nightly GitLab pipeline to check that every SSL certificate that we own is still valid.
Raise a merge request to update the [SSL validator](https://gitlab.com/beautifulcanoe/devops/cron/) to remove the relevant sites from the script.

## Remove DNS entries

Log into our account on [123-reg](https://123-reg.co.uk) (ask the CTO if you need the password).
Go to the **Control Panel -> Choose Domain** and toggle the **Auto renew** button off.

Next, go to **Manage DNS -> Advanced DNS settings** and remove any DNS records that point to Beautiful Canoe servers.

Go back to **Manage Domain** and click on the **Web forwarding** link, and remove any entries for the relevant domain.

That will be all that is necessary for most projects, but before you leave [123-reg](https://123-reg.co.uk) have a look around for any other relevant settings.

## Backup production and staging databases and sites

`cron` scripts on the server should back up both the database and website for any live deployments.
These backups should be in `/var/backups`.
Use `scp` to transfer the latest back ups to your local machine, and put them in the `backups` folder on Dropbox.
Make sure that you check the file sizes of the backups you take, and check that they can be uncompressed.

## Update the server map

Make sure that the relevant hostnames and other entries are updated on our [server map](https://gitlab.com/beautifulcanoe/bc-leadership/management-wiki/wikis/Server-map).

## Removing the server

In general, we prefer not to delete servers, just in case we need a spare server to run occasional jobs, or to help bootstrap a new project.
However, if a server has reached its end of life, back it up and [raise an ITS helpdesk call](https://aston.topdesk.net/) to remove it.

## Archive the Slack channels

Every project should have a **project-project** and **project-gitlab** channel on Slack, for discussion and GitLab notifications.
Once a project has ended, both channels need to be archived.

To archive a channel, go to **Settings cog -> Additional options -> Archive this channel** on the channel page.

## Archive GitLab repo

GitLab repositories can be live, or *archived*.
Archived projects are read-only and do not appear on the list of projects that the group owns.
You will want to do this last, since some of the information on the project (such as CI/CD settings) will not be visible once the project has been archived.

Before you archive a repository, go through any open issues and merge requests, and:

* Remove any assignees
* Remove milestones
* Remove ~Doing, ~"To Do" and ~"Under Review" labels

and perform any basic triage operations that you think are necessary.
This will prevent the [Triage Bot](https://gitlab.com/beautifulcanoe/devops/triage-bot/) trying to perform automated triage tasks on the project, after it is archived.

Next, go to **Settings -> General -> Advanced -> Archive project** and click the **Archive project** button.
