# How to set up a new server

To request a new (virtual) server, create a new issue on Solve.
If the new server needs to be accessible from outside the `aston.ac.uk` domain, be sure to say so on the form.

Each Beautiful Canoe server should have internal access to:

* ICMP, SSH, TCP, UDP, HTTP, HTTPS , LDAP, SMTP, Nagios on ports: 22, 80, 443, 389, 25, 5666
* Local LDAP server
* Local SMTP server
* `bc-monitor.campus.aston.ac.uk` for [Nagios](http://bc-monitor.campus.aston.ac.uk)
* `ssh` from/to Beautiful Canoe staff machines

and external access to:

* TCP / UDP /  HTTP / HTTPS on ports: 80, 443
* [gitlab.com](https://gitlab.com/)
* [packagist](https://packagist.com/) and similar language-specific package repositories
* [letsencrypt.org](https://letsencrypt.org/)
* `ubuntu.com` (for Ubuntu updates and upgrades)
* `api.snapcraft.io` (for Ubuntu upgrades)

Once a new server comes online, there are a number of tasks that need to be carried out to set it up, and automate a number of common administration tasks.
Some of these are common to all servers, and some are only relevant to servers that run live web applications.

## Basic server administration

There are a number of basic tasks that need to be carried out on all servers, regardless of their purpose.

### Find out what external IP address the server has

On the server, run:

```shell
$ curl https://ipinfo.io/ip
134.151.22.7
$
```

and make sure that the IP address is written into the [Beautiful Canoe Server Map](https://gitlab.com/beautifulcanoe/bc-leadership/management-wiki/wikis/Server-map).

### Set up a firewall

On Ubuntu servers, install the Ubuntu firewall with:

```shell
sudo apt-get install ufw
```

!!! Warning
    Once the firewall is enabled, you **must** open port 22 otherwise you won't be able to `ssh` back into the server.

Next enable the firewall and open port 22:

```shell
$ sudo ufw enable
$ sudo ufw allow ssh
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW       Anywhere
22/tcp (v6)                ALLOW       Anywhere (v6)

$
```

## Limit the size of journal logs

Ubuntu journal logs can become very large.
The command `journalctl --disk-usage` can be used to find out how much space the logs are currently taking up, but this can sometimes be several GB.

There are a number of [solutions to this problem](https://askubuntu.com/questions/1012912/systemd-logs-journalctl-are-too-large-and-slow), but we prefer to limit the size of journal logs as they are written.

Open `/etc/systemd/journald.conf` in a text editor and change the line:

```shell
#SystemMaxFileSize
```

to:

```shell
SystemMaxFileSize=200M
```

### Set up Postfix

[Postfix](http://www.postfix.org/) is a mail server for UNIX operating systems.
Follow the [Postfix howto](howto-set-up-postfix.md) to set it up on the new server, so that automated services can email the administrator.

### Ensure the output of cron is read

`cron` automatically runs scheduled tasks and it is important that the server administrator is made aware of any errors that occur during the execution of `cron` jobs.
Edit `/etc/crontab` (or `/etc/anacrontab` on some operating systems), and add this line:

```shell
MAILTO=tech@beautifulcanoe.com
```

For this to work, Postfix must already be working.

### Use cron job to apply OS updates

Operating system updates (particularly security updated) should be applied automatically.

On Ubuntu servers, add a new file `/etc/cron.daily/apt-get-upgrade` with this contents:

```shell
#!/bin/sh

export DEBIAN_FRONTEND=noninteractive

apt-get -qq -o=Dpkg::Use-Pty=0 update
apt-get -qq -o=Dpkg::Use-Pty=0 upgrade
apt-get -qq -o=Dpkg::Use-Pty=0 autoclean

export DEBIAN_FRONTEND=dialog
```

then make it executable:

```shell
chmod +x apt-get-upgrade
```

### Use NTP to set time/date

[Network time protocol](http://www.ntp.org/) ensures that a server keeps the correct date and time.
To set up NTP on Ubuntu, do the following:

```shell
sudo apt-get install ntpdate
sudo ntpdate ntp.aston.ac.uk
sudo timedatectl set-ntp off  # Turn off default Ubuntu time configuration
sudo apt-get install ntp
sudo service ntp restart
```

### Configure apt for future upgrades

To make it easy to [upgrade the server](howto-upgrade-a-server.md) it is useful to configure `apt` to always use the existing configuration for any package it replaces.
Open `/etc/apt/apt.conf.d/local` in a text editor, and add the following text (if it isn't already there):

```shell
Dpkg::Options {
"--force-confdef";
"--force-confold";
}
```

!!! Information
    For more details about why we need to do this, see [this section of the handbook](howto-upgrade-a-server.md#configure-apt-and-dpkg)

### Use NRPE to monitor their health

At Beautiful Canoe, we use [Nagios to monitor our servers](http://bc-monitor.campus.aston.ac.uk).
Follow the [NPRE howto](howto-set-up-nrpe-monitoring.md) to set Nagios up on the new server, and add it to the front-end of the monitoring tool.

### Ensure that Aston users can log in with campus credentials

First, try connecting to the Aston LDAP server with the `ldapsearch` tool of the `ldap-utils` package.
Details on how to do this are not listed here, but please see the `sysadmin-resources` section of the Beautiful Canoe Leadership wiki.

If `ldapsearch` can retrieves Antonio's information, then the next bit is to enable the LDAP module for the Pluggable Authentication Module (PAM) system.
The specific steps change depending on your version of Ubuntu.
[This Ubuntu Community Wiki](https://help.ubuntu.com/community/LDAPClientAuthentication) page has more details.

Particularly, we need to install these packages to get started:

```shell
sudo apt-get install ldap-auth-client nslcd libnss-ldapd
```

The package configuration script will ask for several things:

* A URL;
* a base DN;
* an LDAP version (use v3);
* whether the local root should be the database admin (it should **not**);
* whether the LDAP server requires login (it **does**);
* a username for an unprivileged database user, and
* a password.

these can also be found in `sysadmin-resources` section of the Beautiful Canoe Leadership wiki.

Check that `ldap` is NOT listed in the `/etc/nsswitch.conf` file.
We only want to accept users that are manually added to the system.

You should now be able to SSH into the server with your Aston credentials.

### Creating new admin users

As a minimum, the CTO should be an admin user of *every* Beautiful Canoe server.

For each new users, create a home directory *and* set their shell.
The default shell is `/bin/sh`, but unless the user has asked for a specific shell `/bin/bash` should be used:

```shell
sudo useradd -m -s /bin/bash USERNAME
```

In Ubuntu, the sudoers group is called `sudo`.
You can add the user to that group with:

```shell
sudo usermod -a -G sudo USERNAME
```

At this point, the new user should be able to SSH into the server with their normal Aston credentials.

## Copy over ssh keys

Usually, when you log into a new server with `ssh` you will need to enter a password.
You can avoid this by copying the `ssh` key from your development machine to the server:

```shell
ssh-copy-id USER@SERVER.campus.aston.ac.uk
```

## Serving web applications

To prepare a server to serve a live website, a number of other steps need to be taken.
These are:

* [Set up a GitLab runner](howto-set-up-a-gitlab-runner.md)
* [Automatically backup databases](howto-backup-dbs.md)
* [Use cron to automatically update SSL certificates](howto-update-ssl-certs.md)

You should also add any external URLs to the Beautiful Canoe SSL validator, which runs every night as a GitLab pipeline.
To do this, create a pull request to change the `validate_ssl.py` file in the `cron-scripts` directory of the `sysadmin-resources` repository in the Beautiful Canoe Leadership group.
