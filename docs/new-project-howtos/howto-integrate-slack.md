# How to integrate Slack into a new project

Every new project needs two [Slack](https://beautifulcanoe.slack.com) channels to be created for it:

* `NAME-project`, with purpose **NAME development and maintenance**
* `NAME-gitlab`, with purpose **GitLab events from the NAME project**

For example, the **ACTSS** project has Slack channels **actss-project** and **actss-gitlab**.
Please name the Slack channels according to this convention -- for anyone following a number of projects this means that their channel list is grouped by project name.

!!! important
    If your new project is not a client project, but you are working on a company-wide repository (for example, this [docs.beautifulcanoe.com](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com) repository!) then please do **NOT** create a project channel, and instead use one of the `bc-SUBGROUP-gitlab` channels for GitLab notifications.

## The Slack integrations page

In the project repository page(s) on GitLab go to **Settings->Integrations**.
Scroll down to the bottom of the page and click on the last link, titled **Slack notifications**.

Note that if you cannot see the **Settings->Integrations** option on GitLab, please ask the CTO to do this for you.

You should tick the **Active** checkbox, and the following from the **Trigger** list:

* `Push`
* `Issue`
* `Merge Request`
* `Note`
* `Tag Push`
* `Pipeline`
* `Deployment`

For all of these the channel name is `NAME-gitlab`.

At the bottom of the page, the **Webhook** is a private token that connects GitLab to our Slack workspace.
Take that token from the settings of this repository.
The **Username** should be `gitlab-robot`.

You should **NOT** check the **Notify only...** options, and you should ensure that the integration is enabled for **All branches**.

When you are done, click the **Test settings and save changes button**.
The form should look like this:

![Slack notifications form](./figures/slack-notifications-form.png "Slack notifications form")

## Our Slack app

If your new project will deploy a public URL, you will also need to  please add it to the company dashboard on [uptimerobot](https://uptimerobot.com/) for live monitoring, and ensure that alerts go to `alert@beautifulcanoe.com` and to the relevant `-project` Slack channel (or `bc-online` if your project is internal).
For the Slack alert, you will need to create a new webhook in our [Slack app](https://api.slack.com/apps/AH31R0QH5).
If you don't have access to our [uptimerobot](https://uptimerobot.com/) account, or our Slack app, please ask the CTO.
