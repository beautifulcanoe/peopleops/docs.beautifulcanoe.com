# How to set up a new Trello board

At Beautiful Canoe, we use Trello for client-facing documentation, particularly in the form of [Kanban boards](https://en.wikipedia.org/wiki/Kanban_(development)).

!!! important
    Always avoid using technical words, or software development jargon on Trello boards.
    Each card on the board should be written in a language that the client or an end-user will understand.

We have a standard-set of lists that each new project should start with.
This is more complex than a standard Kanban, because we want to make it clear to the client when a card is waiting on them for testing or more information, and when a card is waiting on us.
Some boards will have extra lists, as appropriate to the project, but each Trello board should start with this standard set of lists:

* **User stories** or you might call this *feature requests*.
* **Bug fixes**
* **In progress** should be regularly updated at each [sprint planning session](../start-here/howto-sprint.md#how-to-start-a-sprint), so that clients can see what has been scheduled for the current sprint.
* **Completed but not on staging server** this usually means work that has been merged into `develop` but is not yet on a release branch.
* **On staging server and waiting for sign-off from client** this is work that is on a feature branch, and waiting on user acceptance testing (UAT).
* **Signed off by client and waiting to deploy to production server** this is work that is on a feature branch and has passed UAT.
* **Completed and deployed to production** i.e. general acceptance (GA).

And you should also add this set of labels:

* **Is fully specified on GitLab Kanban** means the card has a related issue on GitLab, which has been fully specified with a [definition of done](https://www.agilealliance.org/glossary/definition-of-done/). The GitLab issue should be linked somewhere on the Trello card (either in the description or in a comment).
* **GitLab Kanban needs more detail** means that there is a linked issue on GitLab, but it needs more detail before it can be scheduled and assigned to a developer.
* **Waiting on BC for GitLab Kanban to be updated** means that we have not yet created a GitLab issue for this card.
* **Needs more detail from client** means that this card should be discussed in a client meeting, before any more action can be taken.
* **Required for next version** means that the feature request should be scheduled in an upcoming sprint.

You can see an [example client project here](https://trello.com/b/NUkWQ1Ss/example-kanban-for-client-project).

![Add default lists to the new Trello board](./figures/new-trello-board.png "Default lists for new Trello Boards")

## Adding buttons

Trello has a system called [Butler](https://help.trello.com/article/1198-an-intro-to-butler) for automating board tasks.
We use Butler to move cards from one list to another, when we deploy to a staging or production server.

To add these buttons to your new board, click on the **Butler** button on the top right of the board page.

![Trello Butler button](./figures/trello-click-on-butler.png "Trello Butler button")

Select **Board Button** from the menu on the left and click on **Create button**:

![Create a new board button with the Trello Butler](./figures/trello-create-button.png "Create new board buttons with Trello Butler")

Next:

1. Change the gears icon to the robot icon.
1. The title of the button should be **Deploy to production**
1. Click the **Move Cards** option under **Select an Action**
1. The action should move all cards from **Signed off by client and waiting to deploy to production server** to **Completed and deployed to production**
1. Click the green plus button on the right of the action
1. Press the **Save** button on the top right of the pane

To create the **Deploy to staging** button, follow all the steps above, but call the button **Deploy to staging** and move cards from the **Completed but not on staging server** list to the **On staging server and waiting for sign-off from client** list.

Once you've finished, make sure you also enable **Shared with workspace** which allows other workspace users to use the button.

The **Board Buttons** page in Butler should now look like this:

![Edit board buttons with the Trello Butler](./figures/trello-edit-board-buttons-on-butler.png "Edit board buttons with Trello Butler")

Back on your board, your buttons should appear on the top-right of the page:

![Default buttons for new Trello boards](./figures/trello-butler-buttons.png "Default buttons for new Trello Boards")

## Using the board

Once the board has been created, it will need to be regularly updated.
This will mostly happen in sprint planning and before or during client meetings, although ideally boards will be updated whenever a merge request is merged.
Each card on the board should have enough information for the Scrum Master to be able to create a complete use case or bug report, remembering that the card should be written in the language of the product, and understandable to an end-user.

You might use the following templates, which are based on our templates for GitLab issues:

### A template for user stories

```markdown
## User story

**As a**
**I want to**
**So that**

### Scenario

**Given that**
**When I**
**The system will**
**And then**
**And then system**
...
```

### A template for bug reports

```markdown
## What is the current *bug* behaviour?

What is the current, incorrect, behaviour?

## Steps to reproduce the bug

How can a developer or user reproduce the issue?
Please provide detailed steps:

1. Step 1
1. step 2
1. You get it...

## What is the expected *correct* behaviour?

What should happen, when the bug is fixed?
```

## Further reading

* [Example Kanban board](https://trello.com/b/NUkWQ1Ss/example-kanban-for-client-project).
* [Trello Butler](https://help.trello.com/article/1198-an-intro-to-butler)
