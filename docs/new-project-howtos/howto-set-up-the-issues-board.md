# How to set up a project issues board

Every repository in GitLab comes with a list of Issues that allows developers to keep track of new feature requests and bugfix requests.
The issues board for this repository is [here](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/boards) and, like all issue boards in GitLab, you can view it as [a list](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/issues) or as [a Kanban board](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/boards).
We recommend that you mainly use the issues list as a Kanban board, because when the number of issues grows larger, this will make it easier to keep track of where the how your project is progressing.

If you have not come across Kanban before, Atlassian has a [very good explanation](https://www.atlassian.com/agile/kanban/boards).

GitLab issues can be labelled with as many (or as few) custom-defined labels as you wish.
Each label just has a name and a colour associated with it.
GitLab knows which issues should go in which column of the Kanban board by looking for specific labels that define the columns.
When you first create a repository, to set up the Kanban board, you need to tell GitLab to create its default labels for you.

Go to the issue board for your project, and in one of the columns you should see this notification:

![Add default lists to Issues Board](./figures/new-issue-board-labels.png "Default lists for Issues Boards")

Click on the `Add default lists` button, and your Kanban board will be set up correctly.
