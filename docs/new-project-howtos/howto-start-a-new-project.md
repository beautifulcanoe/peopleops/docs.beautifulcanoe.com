# How to start a new project

There are a number of admin jobs to do when a new project is created, so that each new project runs like our current ones.
Most of these jobs are quick and simple, but they all need to be done as soon as the project starts.

!!! Important
    If your new runner will deploy a public URL, please add it to the company dashboard on [uptimerobot](https://uptimerobot.com/) for live monitoring, and ensure that alerts go to `alert@beautifulcanoe.com` and to the relevant `-project` Slack channel (or `bc-online` if your project is internal).
    For the Slack alert, you will need to create a new webhook in our [Slack app](https://api.slack.com/apps/AH31R0QH5).
    If you don't have access to our [uptimerobot](https://uptimerobot.com/) account, or our Slack app, please ask the CTO.
    If your site has an SSL certificate, please also raise an issue and an MR to add it to our SSL validation script in the [cron repository](https://gitlab.com/beautifulcanoe/devops/cron).
    Your SSL certificate should also be [renewed automatically](../infrastructure-howtos/howto-update-ssl-certs.md)

We've put each task in its own file, to make this guide a bit easier to read.
You should set the repository up first, but the other tasks can be done in any order:

* [How to set up a new repository (including documentation)](howto-set-up-a-new-repo.md)
* [How to set up continuous integration and deployment](howto-set-up-ci-cd.md)
* [How to integrate Slack](howto-integrate-slack.md)
* [How to set up the issues board](howto-set-up-the-issues-board.md) for developer-facing [Kanban boards](https://en.wikipedia.org/wiki/Kanban_(development)).
* [How to set up a Trello board](howto-set-up-a-trello-board.md) for client-facing [Kanban boards](https://en.wikipedia.org/wiki/Kanban_(development)).

Lastly, remember to create a merge request to increase the number of projects on the Former Canoeists page on the [`beautifulcanoe.com`](https://beautifulcanoe.com)  site!

## Projects that will live in a new group

The documentation in this section of the handbook assumes that your new project will live within the [beautifulcanoe](https://gitlab.com/beautifulcanoe) group.
If that is not the case, there are a number of extra tasks that need to be completed, to ensure that the new project and group will work with the rest of our automated systems.

If you are starting a new GitLab group, please follow the instructions on the [How to start a new group](howto-start-a-new-group.md) page.
