# How to start a new GitLab group

If you need to start a new GitLab group, there are a number of tasks that need to be completed, to ensure that the new project and group will work with the rest of our automated systems.
These will usually be carried out by the CTO.

## Set-up tasks

1. If at all possible, add a suitable group logo.
1. If the group will hold a public open source project [apply for the GitLab Open Source Programme](https://about.gitlab.com/solutions/open-source/join/).
1. Permanent members of Beautiful Canoe (including @bc-bot) should be added to the new group, at the top-level (i.e. if there are sub-groups, please add members to the super-group). If there are any Fellows or Associates that need to be added, please add these members to individual repositories.

## Adding the new group to our bots

* In the [`cron`](https://gitlab.com/beautifulcanoe/devops/cron/) repository, add the new group ID to the `GROUP_IDS` environment variable.
* In the [`retrospective-bot`](https://gitlab.com/beautifulcanoe/peopleops/retrospective-bot/) repository, add the new group ID to the `GROUP_IDS` environment variable.
* In the [`triage-bot`](https://gitlab.com/beautifulcanoe/devops/triage-bot/) repository, create new environment variables for the new group path and group ID, then raise a new issue / MR to modify the [CI config](https://gitlab.com/beautifulcanoe/devops/triage-bot/-/blob/main/.gitlab-ci.yml) to run the `create_missed_labels.rb` and `sync_group.rb` scripts on the new group, and to apply the triage policies.
* Search through the documentation in this handbook, and add the new group wherever we list all of the company groups.
