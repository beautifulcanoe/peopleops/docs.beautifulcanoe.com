# Your first week at Beautiful Canoe

For your first week, the plan is to develop a set of solid foundations on the basic technologies that we all use in Beautiful Canoe.
Hopefully, you will already have followed [the First Day HOWTO](howto-first-day.md) and signed up to [GitLab](https://gitlab.com/beautifulcanoe/) and our other tools, but if not please do that before continuing here.

In particular, please make sure that you have your [orientation issue](https://gitlab.com/beautifulcanoe/peopleops/orientation/-/issues) open in a browser tab all week, and that you update your progress regularly.
If you think you are blocked on something, or there is anything you don't understand, or you are waiting for another staff member to do something for you, please ask in the `#general` channel on [Slack](https://beautifulcanoe.slack.com/).

!!!Warning
    This page contains a number of pointers to other books and resources.
    Please don't feel you have to read everything in your first week.
    Read what is useful to you now, and come back to this page if you get stuck later on.

## Git

First, you need to learn [Git](../git-howtos/howto-git.md) in order to share your code.
If you have never used Git before, [this Software Carpentry lesson](http://swcarpentry.github.io/git-novice/) is a great place to start.

If you have used Git before, but don't know it so well, make sure you read [Pro Git book](https://git-scm.com/book/en/v2) and work through chapters 1-3 and 5 at some point this week.

### Configuring Git

Before you start doing any significant work, please follow the [Configuring Git HOWTO](../git-howtos/howto-configure-git.md).

## Setting up your text editor or IDE

It is entirely up to you which text editor or IDE you use.
For some projects, the choice will be obvious (for example, Unity projects need to use the Unity editor), but otherwise you are free to choose.
If you are not sure which editor to use, [Sublime Text 3](https://www.sublimetext.com/) is quite straight forward to use, and well-documented.

However, there are some policies that apply across all projects and editors, which are aimed at making code easier to read and review.

!!! policy
    Indents should always be 4 spaces, never tabs.

Please make sure that your editor or IDE is set to automatically use 4 spaces for indentation.

!!! policy
    All whitespace should be stripped from the ends of lines.

It is a good idea to set your editor or IDE to remove *all* trailing whitespace whenever you save a file, that way you don't have to fix whitespace problems manually.
For example, in [Sublime Text 3](https://www.sublimetext.com/) you need to add this to your **Settings**:

```yaml
{
    ...
    "trim_automatic_white_space": true,
    "trim_trailing_white_space_on_save": true,
    ...
}
```

On PHPStorm, the setting is called [Strip trailing spaces on Save](https://www.jetbrains.com/help/phpstorm/settings-editor-general.html) and should be set to **ALL**.

!!! policy
    Every file should end with a single newline.

Sometimes you might come across legacy code in the company that still uses tabs, or has not correctly stripped whitespace from every line.
In this case, please *make a separate commit* to fix the whitespace in the whole file before moving onto the edits you need to make.

## Customise your Bash prompt

You will spend a lot of your time using the command line, especially for working with Git, so it's a good idea to make sure that you know your shell (most likely to be Bash or ZSH) well.
One thing you might want to customise is the prompt, usually just a `$` which tells you that the shell is ready to receive your input.
My shell prompt looks like this:

```shell
(130-write-howto-on-setting-up-bash-prompt) mounts|14:23:32|docs.beautifulcanoe.com|0|$
```

The prompt is split into several parts, some of which you might find useful for your own work:

* `(130-write-howto-on-setting-up-bash-prompt)` is the current Git branch, which is useful when there are many branches / MRs currently in progress - this is provided by a script called `git-sh-prompt` which is installed with Git;
* `mounts` is my username, which is useful if I need to `ssh` into other machines where my username is different;
* `14:23:32` is the current time, this makes it easy to identify processes (e.g. unit tests) that take an unexpectedly long time to run;
* `docs.beautifulcanoe.com` is the basename (i.e. the last part) of the current working directory, and
* `0` is the return value of the last command that I entered (`0` means *success*, anything else is a failure).

![Example customised Bash prompt](./figures/custom-bash-prompt.png "Custom Bash prompt")

To customise your own prompt, you will need to edit the file `~/.bashrc` and then reload the shell configuration by re-sourcing the file:

```shell
. ~/.bashrc
```

!!! Warning
    Some Fellows have found that they are not able to edit their `.bashrc` on Windows.
    If you have the same problem, you can use the file `~/.bash_profile` instead.
    There is a discussion about the difference between these two files on [this superuser question](https://superuser.com/a/409192/115546).

If you are using a Ubuntu machine, you will already have code like this in your `~/.bashrc` file:

```bash
# Set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# Uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
    else
    color_prompt=
    fi
fi

# Enable color support of ls and also add handy aliases.
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
```

Make sure you uncomment the line `force_color_prompt=yes` if you want a coloured prompt.

Next, you probably have some code like this:

```bash
if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt
```

This code sets the prompt, so comment it out, or anything else you have which sets the `PS1` variable.

Next, add this to the end of your file:

```bash
# tput colour table:
#     https://linux.101hacks.com/ps1-examples/prompt-color-using-tput/
if [ "$color_prompt" = yes ]; then
    color_reset=\\[$(tput sgr0)\\]
    color_bold=\\[$(tput bold)\\]
    color_jobs=\\[$(tput setaf 7)\\]
    color_user=\\[$(tput setaf 3)\\]
    color_dir=\\[$(tput setaf 4)\\]
    color_load=\\[$(tput setaf 5)\\]
    color_tstamp=\\[$(tput setaf 6)\\]
    color_succeed=\\[$(tput setaf 2)\\]
    color_fail=\\[$(tput setaf 1)\\]
    sep=\\[$(tput setaf 7)\\]\|
else
    color_reset=
    color_bold=
    color_white=
    color_jobs=
    color_user=
    color_dir=
    color_load=
    color_succeed=
    color_fail=
    sep=\|
fi

# Terminal tab name in gnome-terminal, Guake, etc.
PROMPT_COMMAND='echo -ne "\033]0;$(basename "${PWD}")\007"'
# Installed with the git package, provides __git_ps1 function.
source /usr/lib/git-core/git-sh-prompt
# Note the use of quotes here: "" expand at setting time, '' expand at runtime.
PS1=
PS1+='$('
    PS1+='ret=$?; '  # Save the return code of the last command to be executed.
    PS1+='__git_ps1; '
    PS1+='printf "%s""'  # Print git branch name.
    PS1+=" ${color_user}\u${sep}"  # Username.
    PS1+="${color_tstamp}\D{%T}${sep}"  # Timestamp.
    PS1+="${color_dir}\W${sep}"  # Current working directory (basename only).
    PS1+='"; '
    # Return code of the last command that was executed.
    PS1+='if ((ret == 0)); then '
        PS1+='printf "%s" "'
        PS1+="${color_bold}${color_succeed}"
        PS1+='$ret"; '
    PS1+='else '
        PS1+='printf "%s" "'
        PS1+="${color_bold}${color_fail}"
        PS1+='$ret"; '
    PS1+='fi '
PS1+=')'
PS1+="${color_reset}${sep}"
PS1+='\$ '  # Prompt.
```

and reload the configuration:

```shell
. ~/.bashrc
```

## Creating software at Beautiful Canoe

We have several HOWTOs that guide you through our software development process, which you should read this week:

* [How To Get Code Merged](howto-get-code-merged.md),
* [How To Review Changes](howto-review-changes.md),
* [How To Sprint](howto-sprint.md),
* [How To Use Labels](howto-use-labels.md); and
* [How To Triage Issues](howto-triage-issues.md).

These documents contain a lot of detailed information about how we work, and the most important of them (for this week) is the [How To Get Code Merged](howto-get-code-merged.md) page.
You may well find it overwhelming at first, but once you've have one piece of work merged, the whole process will start to make more sense.

It is likely that you will be working in a very small team, and you may think that our processes are overly complicated.
However, these are standard processes for [agile development companies](https://agilemanifesto.org/), and they make it easier for the CTO to manage all of the projects at once.
Additionally, because all of our developers use the same processes and often similar technology stacks, standardising the projects makes it easier for people on different projects to help one another.

## Your first piece of work for the company

**All**  developers should be listed on [our website](https://beautifulcanoe.com/).
The code for the website is kept in [this repository](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com).
Raise a merge request to add yourself (with a photo) to the website.

!!! important
    Your photo should be square, and reasonably small (it will be rendered at around `108 x 108px`), so please ensure that you crop it, carefully.

Assign the CTO to review your first merge requests, and make sure that you [label them correctly](howto-use-labels.md).

## Install `mdl`

[`mdl`](https://github.com/markdownlint/markdownlint) is a lint (a tool that finds errors in code or documents, without running it) for Markdown.
Almost all our repositories use [`mdl`](https://github.com/markdownlint/markdownlint) to check for consistency in our Markdown documents.
You can see the full list of rules that Markdown lint checks for [here](https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md).

We also have another company-wide rule, which is that all Markdown documents should contain only one sentence on each line.
This makes diffs much easier to read during code review, because the reviewer knows that each addition / subtraction only covers one sentence.

!!! Important
    Text in Markdown files should start each sentence with a new line.

Because we have this extra rule, you will find that almost all repositories (including the repo for this handbook) also have a file called `.mdl.rb` which contains some configuration for [`mdl`](https://github.com/markdownlint/markdownlint).

Because we use [`mdl`](https://github.com/markdownlint/markdownlint) across so many projects in the company it is worth you installing it on your own system.

### Installing `mdl` on a Ubuntu machine

On Ubuntu machines, you will need to do something like this:

```shell
sudo apt-get install gem
sudo gem install mdl
```

!!! Warning
    If you are using Ubuntu 18.04, you may need to install the `rubygems` package rather than `gem`.

### Installing `mdl` on a Windows machine

1. Download Ruby from the [Ruby Installer site](https://rubyinstaller.org/downloads/).
1. Run the installer you downloaded in the previous step.
    * Choose `MSYS2 development toolchain`, when asked by the installer.
    * Choose `MSYS2 base installation` *and* `MSYS2 and MINGW development toolchain`, when asked by the installer.
1. Then open a Windows command line (or restart one you already have open) and run `gem install mdl`.

### Using `mdl`

To run [`mdl`](https://github.com/markdownlint/markdownlint) on the command line, with our configuration, you can do this:

```shell
mdl -s .mdl.rb DOCUMENT.md
```

If you want to run [`mdl`](https://github.com/markdownlint/markdownlint) from your IDE or editor, you will either need to configure it, or find a plugin, such as [this one for Sublime Text](https://github.com/SublimeLinter/SublimeLinter-mdl).

## Summer Fellows only

If you are working on a Summer Fellowship, use what you have learned to raise a merge request to add your own name and project to the [list of Summer Fellows](bc-summer-fellows.md) in this repository.

## Learning about your technology stack

### HTML, CSS, JS

If your project involves building a website, you need to have some basic knowledge of HTML5, CSS and JavaScript.
Go to [MDN](https://developer.mozilla.org/en-US/) and work through the introductory tutorials on HTML, CSS and JavaScript.

Additionally, have a look on [W3School](https://www.w3schools.com/) and [Code Academy](https://www.codecademy.com/).
[W3School](https://www.w3schools.com/) mainly provides theoretical material on main web development technologies, whereas [Code Academy](https://www.codecademy.com/) is more practical, and contains exercises which must be completed in order to progress through the course.
A registration is needed for this (if you wish you can use your Google or Facebook accounts to sign up).

### PHP / Laravel projects

Set up [Laravel Homestead](../php-howtos/howto-setup-laravel-homestead.md) on your development machine.
Go through the [basic tutorial for a link management app](https://laravel-news.com/your-first-laravel-application).

While the [Laravel tutorial](https://laravel-news.com/your-first-laravel-application) gives a basic understanding of how MVC pattern is applied in web app development, it is missing an explanation of how to create a route for newly created form.

To do that, in `routes/web.php` add:

```php-inline
Route::get('/edit', function()
{
  return View::make('submit');
});
```

The code above creates a link that will be used to access the form.
`/edit` is the link that will be used for accessing the form, whereas `'submit'` is the name of a `.blade.php` file that contains the HTML/PHP code for the form.
Now, if you go to `links.test/edit`, in a browser, you will see the **Add link** form created during the tutorial.

### Unity projects

Please follow the [Unity HOWTO](../unity-howtos/howto-setup-a-unity-project.md).

## Learning more about agile development

Over the first few weeks of the summer, we will run an agile study group, looking at different aspects of the software engineering process, but particularly focussing on agile principles.
These meetings will be scheduled in Slack, but you can see the materials that we will be discussing on the [agile study group](agile-study-group.md) page.

Thanks to [Joel Chippendale](https://blog.mocoso.co.uk/2019/10/28/agile-principles-study-group/) for this idea.
