# How to use labels to manage your workflow

As you know from [How to get code merged](howto-get-code-merged.md) all our work in Beautiful Canoe is organised with [issues](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/boards) and [Merge Requests](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/merge_requests).
We use *labels* to help us understand where each issue or MR is in the agile process, how important each task is, and so on.
GitLab provides search tools and issue boards to help with this, so, for example, you could search for all issues in the current sprint that relate to ~"Security Vulnerability".

You may think that this is very picky, but once you have to deal with a project which has a large backlog of tasks, and several developers working on it, you will find that labels are a big help in managing a large volume of information.

## Issue triage

When an issue is raised, it should be [triaged](howto-triage-issues.md), usually by the CTO, although some automated tasks are performed by @bc-bot.
Triage means labelling an issue consistently and, if appropriate, assigning it to a developer and setting its milestone.

!!! policy
    In the rest of this document, Beautiful Canoe policies are written like this.

Most policies are automatically enforced by @bc-bot, you can read through their definitions [in the triage-bot repo](https://gitlab.com/beautifulcanoe/devops/triage-bot/tree/main/policies).

## Label definitions

Labels are defined [here](https://gitlab.com/groups/beautifulcanoe/-/labels) for the `beautifulcanoe` group, [here](https://gitlab.com/groups/feed-quest/-/labels) for the `feed-quest` group, and [here](https://gitlab.com/groups/traffic3d/-/labels) for the `traffic3d` group.
Note that all GitLab groups in the company have the same labels and milestones.

If you are not sure what a given label means, either look it up on the labels page, or just hover over the label to see a description in the tooltip.

Labels fall into a number of categories:

* **Type labels:** ~Bug, ~Discussion, ~Documentation, ~"Feature Request", ~Question,  ~"Refactoring", ~"Infrastructure", ~"Technical Debt"
* **Bug labels:** ~Confirmed, ~Critical, ~"Security Vulnerability"
* **Workflow labels:** ~Blocked, ~Doing, ~"Next Version", ~"To Do", ~"Wont Fix"
* **Priority labels:** ~"Priority:High", ~"Priority:Medium", ~"Priority:Low"
* **Meta-labels:** ~Duplicate, ~"Good First Issue", ~"Help Wanted", ~"Hot Fix", ~"Minor Issue", ~"Tracking"

Projects will sometimes have their own labels, but these should **only** ever relate to product features, and never to workflow.
For example, this repository has labels ~Git and ~PHP.
Project-specific labels are *always* colour-coded as `#428BCA`.

## Type labels

An issue or MR can be a:

* ~Bug a problem with the existing code that prevents a user story from functioning as expected.
* ~Discussion about a problem or proposal for the product, which does not expect to involve making any commits to the repository. When a ~Discussion is closed, the result may be that more issues are raised.
* ~Documentation improvements that do not directly touch code. Remember that by convention, we keep documentation in repositories, never in wikis, and anything that relates to several projects should be included in [these howtos](/).
* ~"Feature Request" a new user story, or part of one.
* ~Question a focussed discussion whose resolution might raise more issues, but will not directly require an MR.
* ~Refactoring a change in the code that improves its structure but does not change its semantics.
* ~"Infrastructure" product-specific changes to be made on a server, in the cloud, or in other infrastructure. May involve configuration but not usually code.
* ~"Technical Debt" changes that improve the quality of the code, but do not directly related to a ~Bug, ~"Feature Request" or ~"Documentation".

!!! policy
    An issue or MR should have exactly one of ~Bug, ~Discussion, ~Documentation, ~"Feature Request", ~Question, ~Refactoring, ~"Infrastructure", or ~"Technical Debt".

## Bug labels

Bugs are categorised more finely:

* ~Confirmed is used when a bug has been reported by a user or Product Owner, and is then confirmed by a project developer.
* ~Critical a bug which actively prevents the user from completing a user story (for example, data not being saved in a DB).
* ~"Security Vulnerability" a bug which might leak or corrupt sensitive information, or allow unauthorised access to a machine.

Anything labelled ~"Security Vulnerability" should normally be marked confidential.

!!! policy
    MRs should never be raised for bugs that are **not** ~Confirmed.
    All bugs should be confirmed within one week of being reported.

## Workflow labels

An issue or MR can be:

* ~Doing currently in progress in the current sprint.
* ~"To Do" to be completed in future or current sprint, but not yet started.
* ~"Next Version" needed by the Product Owner for the next release.
* ~Blocked another issue prevents work on this issue one from progressing.
* ~"Wont Fix" this issue cannot or will not be fixed, and should be closed immediately.

!!! policy
    An open issue or MR should not be labelled as both ~"To Do" and ~Doing.
    An issue labelled ~"Wont Fix" should be closed immediately.
    A closed issue or MR should never be labelled ~"To Do" or ~Doing.

Labels such as ~Missed:2019.1 are created automatically by @bc-bot and should not be created by hand.
These labels mean that an Issue or MR was scheduled for work in milestone [%2019.1 Sprint](https://gitlab.com/groups/beautifulcanoe/-/milestones/1), but was not completed by the end of that milestone.
Please remember that these labels are for feedback on our development process, they are not a judgement about time management.
An Issue might miss a milestone for many different reasons, for example the client may change their priorities, or the work might have to be split into smaller Issues, and so on.

## Priority labels

Priority labels indicate how important a change is to the client.
An Issue or MR can be:

* ~"Priority:High" of critical importance to the client.
* ~"Priority:Medium" should be in a forthcoming sprint, but should not displace a ~"Priority:High" change.
* ~"Priority:Low" an improvement to the project, but should not be scheduled in the next two sprints.

!!! policy
    All issues and MRs should have a single priority label.

## Meta labels

* ~Duplicate an issue is very similar to another one in the repository.
* ~"Good First Issue" a task that would be suitable for a developer who is new to the product.
* ~"Help Wanted" a difficult task with which the MR author or assignee would appreciate help.
* ~"Hot Fix" a patch that needs to be applied directly to the production deployment on the `main` branch, or a staging deployment on a `release/*` branch. Once the MR has been merged, it will also need to be back-ported to `develop`.
* ~"Minor Issue" an issue with low-impact on users and the Product Owner, which has low priority.
* ~"Tracking" an issue with no particular task associated with it, but links to a number of other issues. ~Tracking issues are used where a larger ~Bug fix or ~"Feature Request" has been broken down into smaller tasks.
* ~"Under Review" an issue or merge request is currently waiting on a reviewer, or waiting on a response to a review.

~Duplicate issues should be closed.
However, if there are several issues reporting the same information, choose the one with the most detail and the best description of the task to be completed
