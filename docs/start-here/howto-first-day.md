# Your first day at Beautiful Canoe

Welcome to Beautiful Canoe, it's great to have you on board! :canoe: :tada:

On your first day, you can expect to meet your new colleagues and find out a little more about the company.
Usually, this will be through a process called a Designed Alliance (no spoilers!).

Most new developers will not do any technical work on their first day, but we do ask you to ensure that you have accounts with the tools we use to communicate with one another:

## Slack

[Slack](https://beautifulcanoe.slack.com/) is useful for quick, day-to-day communication.
Slack is split into "channels" which are like chatrooms for different topics. Each project should have two channels -- one for discussion and one for GitLab events.
Make yourself an account on our [Slack](https://beautifulcanoe.slack.com/), and add your photo.
You will automatically be added to a number of channels, including:

* `bc-online` for discussion of company-wide projects, such as this engineering handbook and the company website,
* `bc-devops-gitlab`, `bc-identity-gitlab`, `bc-peopleops` which are read-only channels showing events from our company-wide GitLab repositories, and
* `bc-twitter` a read-only channel showing activity on our [Twitter account](https://twitter.com/beautifulcanoe).

You will also need to ask the CTO to add you to the channels that are specific to your client project, which will be named `PROJECT-project` (for discussion) and `PROJECT-gitlab` (for events on GitLab).

## GitLab

[GitLab](https://gitlab.com/beautifulcanoe/) is where we keep all our code and technical documentation.
Get an account and tell the CTO your username on Slack.
They will add you to the [Developers](https://gitlab.com/beautifulcanoe/developers) group and any other repositories that you will need.

Note that as of May 2021, GitLab requires users to [validate their account with a credit card](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/).
Your credit card will not be charged, but you *must* do this, otherwise some GitLab features will not work for you.
This change is due to GitLab pipelines being used for cryptomining.

### Your orientation issue

Before you joined us, we have been busy preparing to welcome you to the company, and in the [orientation](https://gitlab.com/beautifulcanoe/peopleops/orientation) repository you should find an [issue](https://gitlab.com/beautifulcanoe/peopleops/orientation/-/issues) with your name on it.
This issue will have a list of tasks for you to complete this week, as well as tasks for other members of the company to finish - it will be based on a template from [this repository](https://gitlab.com/beautifulcanoe/peopleops/orientation-templates).

Make sure that you keep a browser tab open for this issue, and that you update your progress regularly.
If you think you are blocked on something, or there is anything you don't understand, or you are waiting for another staff member to do something for you, please ask in the `#general` channel on [Slack](https://beautifulcanoe.slack.com/).

### Gravatar

Although not strictly necessary, it is a good idea to have a [Gravatar](https://en.gravatar.com/) associated with the same email address you used to sign up to [GitLab](https://gitlab.com/beautifulcanoe/).
This will be the picture that will be associated with all your commits on [GitLab](https://gitlab.com/beautifulcanoe/).

## Trello

[Trello](https://trello.com/) is used for communication with clients and other non-technical documentation.
Make sure you have an account there and ask the CTO to add you to the relevant boards.

## Our company website

[Our website](https://beautifulcanoe.com/) is a window to the outside world, please make sure that you are familiar with its contents.
Later this week you will add yourself to our list of employees on the website.

## What happens next?

Next, please work through the [First Week HOWTO](howto-first-week.md).
