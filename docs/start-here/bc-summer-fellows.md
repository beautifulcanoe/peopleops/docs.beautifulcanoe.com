# Beautiful Canoe summer fellows

This is a list of all of our Summer Fellows and the projects they worked on.

If you are a Fellow, you should [create a merge request](howto-get-code-merged.md) to add your name and project to this document in your first week.

## 2022

* Lionel Muskwe. **Project:** LinguaSnapp Brum

## 2021

* Rajan Malhi. **Project:** Forensics Voice Comparison System
* Parminder Saini. **Project:** FoLD
* Dorian Mayamba. **Project:** Computer Science Industry Club Portal

## 2020

* Abdiyee. **Project:** Text Crimes and FOLD
* Ehsanullah Afzal. **Project:** Text Crimes and FOLD

## 2019

* Callum Bugajski. **Project:** Traffic3D
* Johnny Le. **Project:** Text Crimes
* David Powell. **Project:** Text Crimes
* Adam Collins. **Project:** Aston Contractor and Tradesperson Sign-In System
* Asim Mahmod. **Project:** Aston Contractor and Tradesperson Sign-In System

## 2018

* Abdiyee Idris. **Project:** Security improvements for all projects
* Adil Nazir. **Project:** Aston Prescription Learning Environment
* Alex Ulpiani. **Project:** Security improvements for all projects
* Arun Bahra. **Project:** Aston Contractor and Tradesperson Sign-In System
* Ben Gladstone. **Project:** Aston Contractor and Tradesperson Sign-In System
* Chloé Alsop. **Project:** Think Beyond Data
* Daniel Sutton. **Project:** Text Crimes
* Gavinder Hayer. **Project:** Beautiful Canoe Website and Infrastructure
* Gerard Alinas. **Project:** Aston Contractor and Tradesperson Sign-In System
* Juozas Beniusis. **Project:** Aston Contractor and Tradesperson Sign-In System
* Kamran Ali. **Project:** Beautiful Canoe Website and Infrastructure
* Paul Aina. **Project:** Beautiful Canoe Website and Infrastructure
* Rajan Malhi. **Project:** Aston Prescription Learning Environment
* Rebecca Coleman. **Project:** Text Crimes
* Shibu George. **Project:** Text Crimes

## 2017

* Adeel Ahmed. **Project:** Activity Model System
* Adil Nazir. **Project:** Aston Prescription Learning Environment
* Adnaan Hussain. **Projec:t** Aston Prescription Learning Environment
* Alex Luckett. **Project:** Senior Developer
* Amandeep Kauldaur. **Project:** Exceptional Circumstances System
* Amir Makanvand. **Project:** System and Server Administration
* Bruno Simao Zorima. **Project:** Activity Model System
* Chloe Alsop. **Project:** Beautiful Canoe Website
* Christopher Ting. **Project:** Aston Contractor and Tradesperson Sign-In System
* David Bosun-Arebuwa. **Project:** System and Server Administration
* Dimitrios Karakatsanis. **Project:** Computer Science Industry Club Portal
* Jordan Lees. **Project:** GRACE: Student Mental Health Companion
* Kamran Ali. **Project:** Beautiful Canoe Website and Marketing
* Kelsey Chan. **Project:** Text Crimes
* Matthew Chambers. **Project:** Aston Prescription Learning Environment
* Mohammed Nazish SAGHIR. **Project:** Beautiful Canoe Website
* Moscope Thompson. **Project:** Exceptional Circumstances System
* Nathaniel Baulch-Jones. **Project:** Text Crimes
* Romario Giacholari. **Project:** Computer Science Industry Club Portal
* Sam Bangura. **Project:** Text Crimes
* Satpal Singh. **Project:** NHS Predictive Modelling System
* Zeshan Wajid. **Project:** Activity Model System

## 2016

* Callum Kendrick. **Project:** Text Crimes
* Clare Buckley. **Project:** Text Crimes
* Farooq Ilyas. **Project:** Activity Model System
* Hamzah Khan. **Project:** Work-Based Learning System
* Jamie Ingram. **Project:** Aston Prescription Learning Environment
* Kiel Pykett. **Project:** Work-Based Learning System
* Lewis Blackburn. **Project:** Text Crimes
* Meena Hoda. **Project:** Computer Science Industry Club Portal
* Reece Brend. **Project:** Activity Model System
* Peter Lawlor. **Project:** Computer Science Industry Club Portal
* Scott Street. **Project:** Aston Prescription Learning Environment
* Twba Al-Shaghdari. **Project:** Activity Model System
* Ashley Bridgwood. **Project:** APLE
* Aman Soni. **Project:** FLDB

## 2015

* Callum Kendrick. **Project:** Text Crimes
* Aaron Tello-Wharton. **Project:** Programmer
* Hasan Tariq. **Project:** Forensic Linguistics Database

## 2014

* Assa Singh. **Project:** APLE
* Ken Earl. **Project:** APLE
* Zara Ahmed. **Project:** Aston Navigation
* Bhalchandra Wadekar. **Project:** Firefly Android App
* Akeeb khan. **Project:** Feedback System
* Leon Jansen Van Rensburg. **Project:** Module Database

## 2013

* Tara Ojo. **Project:** Birmingham and Solihull LEP white paper pharmacy prescription trainer & languages department DVD library

## 2012

* Edward Hull. **Project:** Library inventory system Forensic analytics app for West Midlands police
