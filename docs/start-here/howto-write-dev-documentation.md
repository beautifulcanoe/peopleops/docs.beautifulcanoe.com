# How to write developer documentation

!!! acknowledgements
    This HOWTO is based on work by [Megan Robertson](https://www2.aston.ac.uk/eas/staff/a-z/megan-robertson).

Ideally, you will start your documentation when you start your project and keep adding to it bit by bit!
What should be documented:-

* What you are trying to accomplish;
* how you make it happen, and
* how and why you wrote your code the way that you did.

The aim is to give someone else a clear understanding of how your programme actually works, and show them the thought processes behind the design and development of your code.

Ideally no Beautiful Canoeists will be run over by a bus, but the idea of technical documentation is that someone else could pick up your project and carry on!
Most Beautiful Canoe projects run for several years, so good documentation should make it easy to hand over to the next developer ... which may well be you if you come back to work for us in a second contract, or even if you come back to a complex piece of code after a break.

## Where should your documentation go?

!!! policy
    Every repository should have [standard documentation](../new-project-howtos/howto-set-up-a-new-repo.md#add-standard-documentation) which describes what the repository is for, which client commissioned the project, the student developers that have worked on the project (in `README.md`) and how to start working on the repository (in `CONTRIBUTING.md`).
    Any more detailed documentation should be checked into version control in a directory called `docs/`.

Every Beautiful Canoe repository should have some [standard documentation](../new-project-howtos/howto-set-up-a-new-repo.md#add-standard-documentation) in `README.md` and `CONTRIBUTING.md`.
Beyond that, you will have three sorts of documentation:

1. *In-code* documentation, such as Javadoc-style comments. This should be as sparing as possible -- don't add documentation to every function or method just for the sake of it! Ideally your code will be easy to read, so you should only add comments when something important is *not* directly obvious in the code.
1. Detailed documentation that is separate to the code -- for example, designs for databases or security policies, or descriptions of server configurations. These documents should be written in [Markdown](https://docs.gitlab.com/ee/user/markdown.html) and stored in a directory called `docs/` in your repository.
1. Documentation from meetings with the client or other users. These should be stored in [Trello](https://trello.com/), [Google Drive](https://drive.google.com/) or similar, and the CTO should have ownership rights to the documents (so that they can be shared with other student developers).

!!! policy
    Documentation that might be useful to more than one project should be checked into [this repository](https://gitlab.com/beautifulcanoe/peopleops/docs.beautifulcanoe.com/) and individual projects should link to the documentation on [this website](https://docs.beautifulcanoe.com/).

## Who should write your documentation?

Ideally someone else should write the technical documentation for your code, but in this imperfect world you'll probably have to do it for yourself.
This is because you know how your code works and why you wrote it the way you did ... so will leave out things that are just so darn obvious that they don't need stating (but which are completely incomprehensible to anyone else).

IMAGINE ... that you are sitting explaining your code to another programmer.
Tell them why you wrote it the way you did, what tricks you employed, what links to where, and so on.
Now write that down, and you have made a good start on your technical documentation.

This is techie-to-techie stuff, so you don't have to be formal -- but you do need to be clear and unambiguous.
Use diagrams, sketches, snippets of the code itself, screenshots: whatever you think will help give someone else a glimpse into your mind when you developed the code.
Good luck!
Some people see documentation as tedious, but you'll soon see that you are cleverer than you thought you were as you explain all that neat and elegant code!
It may help you develop as a coder too, as you think about why you did something that you did just `cos it worked!
