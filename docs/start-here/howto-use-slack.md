# How to use Slack for team communication

To keep track of important team information, make announcements and just generally chat, we use a messaging service called [Slack](https://beautifulcanoe.slack.com/).
It's easy to use, and its channel system separates out discussions on different topics, you can easily find information.
You can download both a desktop, [Android app](https://play.google.com/store/apps/details?id=com.Slack) and/or [iPhone app](https://itunes.apple.com/app/slack-app/id618783545?ls=1&mt=8) so you don't miss anything important -- just make sure that your notifications are turned on!

To create an account, just go to the [team page](https://beautifulcanoe.slack.com/) and use your Aston email address.
If you have any difficulties creating an account, ask the CTO for advice.

## Installing Slack

To install Slack for your platform, head to the [download page](https://slack.com/intl/en-gb/downloads/) and follow the instructions there.

## Enabling notifications on the web and desktop apps

When you first log in, you should get a banner alert asking you to enable desktop notifications.

![slackWebAlertBanner](./figures/slackWebAlertBanner.png)

A pop up message will appear to confirm, click **Allow**, and you now have desktop notifications enabled!

If you're not prompted, click on **Beautiful Canoe** in the top-left of the app, and then **Preferences** to open the **Notifications** tab.

![slackWebNotifications](./figures/slackWebNotifications.png)

!!!Important
    We do not expect developers to work overtime at Beautiful Canoe.
    It is a good idea to disable Slack notifications out of office hours (09:30--17:30).
    Go to **Preferences -> Notifications -> Do not disturb** to do this.

## Channels

The list of channels you have joined can be found in the sidebar.
You will automatically be signed up to a number of channels -- please do not remove yourself from these!
Channels allow us to separate conversations out, so that developers are not interrupted by discussion that isn't relevant to them.

There are a number of channels you will definitely need to know about:

* `#general` is for general all-company conversation, and `#random` is for non-work discussion.
* `#team-wins` is for celebrating anything particularly cool that has happened in the company.
* `#agile-daily-stand-up` is the channel for the stand-up meeting that starts each working day.
* `#agile-retrospective` is for the demo and retrospective meeting that ends each sprint.

In addition, each project has two channels:

* `#project-gitlab` a channel where GitLab notifications are sent from the project repositories -- you can see issues that are opened and closed and activity on all merge requests here. You can also see passed and failed pipeline notifications, so you don't have to sit staring a pipeline progress once you've pushed code to a shared repository.
* `#project-project` is for general discussion about the project.

When you start working at Beautiful Canoe, the CTO should sign you up to the relevant project channels.
There are also two channels:

* `#bc-gitlab`, and
* `#bc-project`

for company-wide projects, such as this engineering handbook, and the company website.

### Joining channels on the desktop

To join additional channels, click **Channel browser**.
![slackWebChannelHover](./figures/slackWebChannelHover.png)

All non-private channels will be listed here.
Join as many as you like, leave as many as you like, but make sure that you are subscribed to the shared channels listed above, and the channels for your client project.

### Creating channels on the desktop

!!!Warning
    In general, we try not to create extra channels.
    Each new channel will will use up some of the time an attention of the developers in it -- our most important company assets!

Click the plus button next to **Channels**.
Choose whether it's public or private, set the name and purpose, then send your invites!

## Direct messages

You can send direct messages in Slack.
To do so, just open the side bar and you should see a **Direct messages** section.
To start a new DM, click the plus arrow and select who you want to talk to.

## Pinned messages

You can pin messages and files to channel, so you can easily access important information without having to search through the chat history.
If you pin something, it will be pinned for everyone, so please only use this for things everyone might want to find again in the future.

### Pinning a message

Hover over the message you want to pin, and icons will appear in the top right of the message.
Select the three dots, and you'll have the option to **Pin to channel**.

![slackWebPin](./figures/slackWebPin.png)

### Viewing pinned messages on the desktop

At the top of every channel or DM, you'll see a pin symbol.
Click it to open up a sidebar containing all of messages pinned to that channel/chat.

![slackViewPinnedMessages](./figures/slackViewPinnedMessages.png)

## Slackbot

Slackbot can be used to automatically send messages at certain times, or in response to keywords.
You can also DM Slackbot to ask it questions about Slack features.
You can create custom Slackbot responses (in addition to custom emojis and load screen messages) by selecting **Customize Beautiful Canoe**.

![slackWebCustomiseSlack](./figures/slackWebCustomiseSlack.png)
