# How to sprint at Beautiful Canoe

In Beautiful Canoe, all project work is broken down into *sprints*, which:

* run company-wide, that is, all projects start and end their sprints on the same day;
* (usually) last for two-weeks;
* start with a rough plan, based on the priorities of the client;
* include a release, either to a staging server or production server,
* end with a [demo and retrospective](http://retrospectivewiki.org/index.php?title=Retrospective_Plans).

## Before the sprint

Before each sprint a new [milestone](https://gitlab.com/groups/beautifulcanoe/-/milestones) should be created for that sprint in the [`beautifulcanoe`](https://gitlab.com/groups/beautifulcanoe/) group.
Bob the Bot will then copy the new milestone definition into the [`feed-quest`](https://gitlab.com/groups/feed-quest/) group and the [`traffic3d`](https://gitlab.com/groups/traffic3d/-/milestones) group, overnight.
There should be *no* project-level milestones in the company.

!!! important
    Sprint milestone *must* have start and end dates and must be of the form `YYYY.NN Sprint`, so that the automated sprint tools that we use at the end of each sprint can run correctly.

The [Scrum Master](https://www.scrum.org/resources/what-is-a-scrum-master) (usually the CTO) should ensure that each project has an accurate, up to date, and correctly [triaged](howto-triage-issues.md) issues board.

In particular, any issues that were left unresolved from the previous milestone should be rolled over to the next sprint.

## How to start a sprint

Each sprint begins with a planning sessions.
Each project will usually have its own session, which should take place between the project developers and BC Scrum Master.
The sprint plan should be based on:

1. the priorities of the client, as expressed in the most recent meeting.
1. the long term need to avoid or tackle [technical debt](https://martinfowler.com/bliki/TechnicalDebt.html).

Planning meetings are driven by the Trello and GitLab [Kanban boards](https://en.wikipedia.org/wiki/Kanban_(development)) for the project, which should contain the user stories and bug reports for the project.
In order for planning meetings to be useful, and for the needs of the project to be accurately represented, issue lists should be kept up to date, and correctly [labelled](howto-use-labels.md).
The issue boards for [`beautifulcanoe`](https://gitlab.com/groups/beautifulcanoe/-/boards), [`feed-quest`](https://gitlab.com/groups/feed-quest/-/boards) and [`traffic3d`](https://gitlab.com/groups/traffic3d/-/boards) should be an accurate representation of the status of each project, at any given time.

During the planning session, each issue that is considered for scheduling should be discussed in detail, and the discussion should be added to the issue in comments.
These discussions should ensure that each developer on the project knows how to start the issue.
Sprint planning may involve looking in detail at the code base, or reproducing bugs in the product, to clarify the steps that need to be taken to resolve the issue.
This process should not be rushed, and developers should be encouraged to ask questions.

By the end of the planning meeting:

* issues to be resolved in the current sprint should be labelled ~"To Do";
* issues to be resolved in the current sprint should be marked with the current milestone;
* each issue labelled ~"To Do" or ~Doing should be assigned to a Developer;
* any issue that has been removed from the sprint should either be assigned to a future milestone or the [backlog](https://gitlab.com/groups/beautifulcanoe/-/milestones/12)
* most relevant issues should have a [priority label](howto-use-labels.md).

The ~"To Do" list should not extend beyond the time of the current sprint, and care should be taken not to overload any of the developers.

!!! important
    Developers should remember that sprint planning is intended to set priorities and coordinate team members -- it is not a target to reach, and no one should work overtime to "finish" the work in the backlog.

### Planning poker

Optionally, sprint planning may use [planning poker](https://en.wikipedia.org/wiki/Planning_poker).
This is an agile tool, whereby team members *bid* for how long they believe each issue will take to resolve.

The Scrum Master should chair planning poker, and not take part themselves.
The chair, the client, or someone representing the client, should give an overview of each user story or issue, and this should be discussed until each team member feels they understand the story in full.
At this stage, numbers and estimates should *not* be discussed.

Next, each developer offers an estimate for how long they believe the issue will take to resolve.
Since Beautiful Canoe sprints only last two weeks, it is sensible to give estimates in hours.
If every team member believes that the issue cannot be resolved in a reasonable number of hours, the issue should be labelled ~Tracking issue and broken into smaller sub-issues.
Each estimate should be a number of hours in the Fibonacci sequence:

> 1, 1, 2, 3, 5, 8, 13, 21, ...

Developers should reveal their estimates at the same time.
There are a number of planning poker tools and apps which enable this, but the simplest technique is for each person to hold up a number of fingers representing their estimate.

Developers with very high or low estimates are given a chance to justify for their estimate and discuss them with the team.
Discussion should continue until the team has reached a consensus.
The developer who is most likely to be assigned to the issue has the largest portion of the *consensus vote*.

## During the sprint

During the sprint each developer is responsible for resolving the issues that they are assigned to, using [our usual process](howto-get-code-merged.md).
However, sprint plans are not set in stone, they are *living documents* that should be reviewed and updated (with the Scrum Master) as the sprint continues.

Developers should ensure that the issues and merge requests that they are working on are [correctly labelled](howto-use-labels.md), so that the whole team can coordinate well.

Developers are encouraged to [pair](https://en.wikipedia.org/wiki/Pair_programming) on their issues at any point, if they find that style of development useful.

If it seems likely that the original sprint plan over-estimated or under-estimated the amount of work that could be completed, or if the client changes their priorities, the issues board should be updated.
The Scrum Master should regularly review the progress through the [current milestone](https://gitlab.com/groups/beautifulcanoe/-/milestones) and re-evaluate the backlogs and estimates where appropriate.

## The feature freeze

On the Wednesday afternoon of the second week of the sprint each project should hold a *feature freeze*.
At this point, a deployment to the staging server should be made, and the client should be informed.
From this point to the end of the sprint, only ~"Hot Fix" changes (primarily ~Bug fixes and improvements to ~Documentation) should be made, and work on the rest of the backlog should stop.

Every member of the team, including the client and Scrum Master, should QA test the new features in the staging environment.
A new ~Tracking issue should be created, to keep track of changes that need to be made before the release candidate can be merged into the `main` branch.
[This issue](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com/issues/60) is a good example of a tracking issue for a release candidate.

## How to end a sprint

Ideally, a sprint will end with a deployment to the *production* environment on the `main` branch.
In this case, a new issue should be raised in the project, to *backport* ~"Hot Fix" changes from the `release` branch (or, exceptionally, the `main` branch) to `develop`.

This will ensure that `develop` is kept up to date with all changes that the client will see in the production environment.

## How to reflect on a finished sprint

At the end of the second Friday of the sprint, after any relevant deployments have been made, a whole-company demo and retrospective will take place.

### Before the retrospective

To prepare for a retrospective, the Scrum Master should run the [retrospective bot](https://gitlab.com/beautifulcanoe/peopleops/restrospective-bot) which will create an issue in the [Team Retrospectives](https://gitlab.com/beautifulcanoe/peopleops/bc-retrospectives/) repository which will summarise what happened during the sprint.

To run the bot, see the [instructions in the retrospective-bot repository](https://gitlab.com/beautifulcanoe/peopleops/retrospective-bot/blob/main/CONTRIBUTING.md).
The issue generator can either be run as a manual pipeline on that repository, or as a command-line task:

```shell
git clone git@gitlab.com:beautifulcanoe/peopleops/retrospective-bot.git
cd retrospective-bot
bundle install --path=vendor/bundle
bundle exec ruby retrospective.rb \
    --dry-run \
    --token=${SECRET_TOKEN} \
    --milestone="2019.1 Sprint"
    --groups=${GROUP_IDS}
```

The secret token can be obtained by either looking at the CI/CD variables in the repository, or logging in as @bc-bot and looking through the Personal Access Tokens in his settings.
The group ids can be obtained either from the CI/CD variables in the repository, or from the group pages on GitLab.

Once the sprint is over, @bc-bot will label issues that are still open, with a label such as ~Missed:2019.1

!!! important
    Developers should remember that issues are labelled to help with sprint planning and estimation -- an issue that is rolled into the next sprint is not a judgement on the ability or competence of the assigned developer.

[This issue](https://gitlab.com/beautifulcanoe/peopleops/bc-retrospectives/-/issues/1) is an example of a retrospective summary.
This summary information should feed into a face-to-face retrospective, and can be used to generate summaries of new features and bug fixes to report back to clients.

Any [style of retrospective](http://retrospectivewiki.org/index.php?title=Retrospective_Plans) may be used, but each session should be a safe space for team members to discuss their reflections on their work in a non-judgemental atmosphere.
For remote retrospectives, we usually set up a [Metro Retro](https://metroretro.io/) board, to be used in a video call.

Before the end of lunchtime on the Friday, links to the retrospective issue and board and details of the video call should be posted to the [#agile-retrospectives](https://beautifulcanoe.slack.com/archives/C010Z4A0E9H) channel on Slack.

### During the demo

Sprint demonstrations will take place directly before the retrospective.
During the demo, team members should take turns to share their screen (if online) or use a large screen as a second display (if in the office).

Each team member will walk through the work they have completed during the sprint from a _user_ or _client_ perspective (that is *no* code should be shown in the demo).
If a team member has worked on more than one product in the sprint, they should demonstrate their work in all products.

Team members should use the latest production / staging deployments of their product for the demo, and only use local deployments as a fallback.

### During the retrospective

The retrospective will directly follow the demonstration.

During the retrospective, the team will usually:

1. Write down anything they want to reflect on (positive, negative or neutral) that has happened during the sprint.
1. Read through the concerns of the rest of the team.
1. Group the team's concerns into themes.
1. Vote on which groups need to be discussed.
1. Discuss the most popular concerns and generate ideas for improving future sprints.

During this process, it is important that everyone can speak honestly, in an atmosphere that is open and non-judgemental.
It is useful to keep in mind the *retrospective prime directive*:

> Regardless of what we discover, we understand and truly believe that everyone did the best job they could, given what they knew at the time, their skills and abilities, the resources available, and the situation at hand. --Norm Kerth, Project Retrospectives: A Handbook for Team Review

### After the retrospective

The Scrum Master should ensure that the retrospective board is added to the relevant [Team Retrospectives](https://gitlab.com/beautifulcanoe/peopleops/bc-retrospectives/) issue.
If the whiteboard in the Beautiful Canoe offices has been used, then this should be photographed.

Every retrospective should result in a concrete set of actions that team members have chosen to act on.
The Scrum Master should ensure that these are documented in a suitable place (e.g. GitLab issues for anything technical, including documentation).
Next-actions should be followed-up, to ensure that retrospectives result in continuous improvement.
This may mean that in the next retrospective, the team looks back at the notes from the last retrospective.

## The first and last sprints of the calendar year

If you are participating in a sprint around the turn of the year, there are some extra tasks that the Scrum Master should complete.
These should be listed in an issue in the [Roadmap](https://gitlab.com/beautifulcanoe/devops/roadmap) repository with a title similar to **Annual cleanup tasks** (see [the 2021 example](https://gitlab.com/beautifulcanoe/devops/roadmap/-/issues/31)).

As a minimum, this task should include:

* Ensuring that milestones for the outgoing year are closed and milestones for the incoming year are scheduled.
* Ensuring that the shared Outlook calendar contains all scheduled sprints and Scrum ceremonies.
* Tidying the [Team Review](https://gitlab.com/beautifulcanoe/peopleops/bc-retrospectives) issues.
* Creating a new issue in [Roadmap](https://gitlab.com/beautifulcanoe/devops/roadmap) for the coming year.

## Further reading

* [How to use GitLab for agile development](https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/) -- note that this includes some paid features, such as epics and weights, that we do not use.
* [Kanban](https://en.wikipedia.org/wiki/Kanban_(development))
* [GitLab automatic retrospectives](https://about.gitlab.com/2019/03/07/how-we-used-gitlab-to-automate-our-monthly-retrospectives/)
* [GitLab release manager how-to](https://gitlab.com/gitlab-org/release/docs/blob/master/quickstart/release-manager.md)
* [Metro Retro](https://metroretro.io/)
* [Retrospective plans](http://retrospectivewiki.org/index.php?title=Retrospective_Plans)
