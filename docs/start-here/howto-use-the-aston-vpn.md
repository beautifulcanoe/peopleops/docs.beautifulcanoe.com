# How to connect to the Aston VPN

## Request VPN access

If you need access to the Aston VPN, please [fill out this form](https://www2.aston.ac.uk/ict/staffguide/desktop-support/remote/vpn-info).

## VPN client settings

On Debian-based systems, install [Open Connect](http://www.infradead.org/openconnect/), this example installs the Gnome based front-end:

```shell
sudo apt-get install network-manager-openconnect-gnome
```

Go to the *network manager* in your *settings* and add a new VPN.
The settings should be:

* **VPN Protocol** Cisco AnyConnect
* **Gateway** `secure.aston.ac.uk`
* **CA Certificate** None
* **Proxy** None
* **User certificate** None
* **Private key** None
* **Token mode** TOTP -- manually entered

If you have not been given the secret token by IT Services, please see the CTO.
