# How to work remotely at Beautiful Canoe

The vast majority of our technical work can take place remotely.
However, some parts of the work will become more difficult, and communicating well is crucial to making remote work productive.

!!! warning
    Whilst the COVID-19 situation progresses, it is likely that this page will be updated, as we learn how best to run our processes online -- so please keep an eye out for changes here!

## Communications

Communicating *well* remotely is not as easy as it sounds.
There is a large amount of information flowing through the company at any given time, and managing this well makes it easier for everyone to move their own work forward.

Equally important, is ensuring that both you and your colleagues know what everyone is working on.
This is sometimes called *working out loud* and it's much more important in a remote environment, where it's a lot more difficult to coordinate your work.
It also fits well with our company [value of openness](https://beautifulcanoe.com/about#values).

It is a good rule of thumb to never use private messaging, unless you need to discuss something genuinely private (e.g. an absence from work or an issue with a client).
All technical work should be discussed either on GitLab issues / MRs (which helps us preserve the history of each project) or on Slack for less formal conversations.

This [podcast from Collaboration Superpowers](https://www.collaborationsuperpowers.com/125-management-by-working-out-loud/) contains a much longer discussion on working out loud, which you may find useful.
It is worth reflecting on which of the skills discussed on that page you think we do well at Beautiful Canoe, and which you think we could improve.
Please do bring your thoughts to a [sprint retrospective](howto-sprint.md#how-to-reflect-on-a-finished-sprint).

### Using Slack well

We use [Slack](https://beautifulcanoe.slack.com/) for all day to day asynchronous communication.
Please familiarise yourself with the [Slack HOWTO](howto-use-slack.md), and follow these tips for using Slack well:

* Have a photo of yourself on your profile.
* Set an appropriate *status* when you are away for lunch or away from your keyboard for longer than a coffee break, so that colleagues know that you won't be as responsive as usual.
* Try to use the right channel for each conversation. Keep project discussion to the `#PROJECT-project` channels and the stand-ups in `#agile-daily-stand-up`. Feel free to move to a different channel if the conversation drifts. This is helpful when you need to come back to search for something that has previously been discussed, and avoids developers being overwhelmed by information that isn't relevant to them.
* Keep conversations about MRs on the MR itself, so that other developers can see the history of each change in the code-base.
* Save conversations that you might need to come back to later, and consider keeping the [Saved items](https://slack.com/intl/en-gb/help/articles/360042650274-Save-messages-and-files-) sidebar open.
* Avoid email wherever possible, and only use it for very long form discussion.

### Getting technical help when you are stuck

If you are stuck with your technical work and cannot progress, please use [Slack](https://beautifulcanoe.slack.com/) in the first instance.

### Avoid adding more communication tools

There are a large number of tools available to help people who work remotely, or to "manage" agile projects.
It's always tempting to adopt the newest, shiniest app, or to use the same processes and stack as your favourite company.
Try to avoid this impulse!
The stack we use now may not be perfect, but we want to reduce the amount of unnecessary task switching, and switching between apps and avoid developers feeling overwhelmed by a firehose of information.

Currently we use:

* [Slack](https://beautifulcanoe.slack.com/) for day to day communication;
* [Trello](https://trello.com/) for Kanban boards that are *suitable for clients to use*;
* [GitLab](https://gitlab.com/) for developer Kanban boards, DVCS management (i.e. git), DevOps (i.e. pipelines) and code review, and
* Email and Outlook calendar for client communication (ideally *only* client communication) and booking meetings.

This is already quite a large number of communication channels, especially for colleagues who need to monitor *every* project.
So, if we do add another tool, we need to be sure that it's adding something of real value, that we can't already do with the current stack, and that it won't cause problems or incur unexpected costs in the future.

## Staying healthy

Many people find it more difficult to stay healthy whilst working remotely, as they no longer have the structure of having to work on-campus with their peers, or a strict separation between their home and work environments.
It is useful to be *intentional* about your health, and to give some thought to how remote working might affect you.
For example, how will you exercise when you are working at home?

This section contains some tips that others have found useful, but you know best what will work for you.

### Your working environment

Most people find it useful to separate their working environment from their leisure environment.
If you do have to work in your living room or bedroom, try to find some other way to separate work from non-work.
For example, packing your laptop away at the end of the day, or only using a corner of the room for work.
The aim here is to be able to think about work (and mainly only work) during office hours, and to not think about work at all in your own time.

Ideally, your working environment should be quiet enough to allow you to concentrate and somewhere where you can avoid unexpected interruptions.
If it helps, you might want to buy a [meeting in progress sign](https://www.amazon.co.uk/s?k=meeting+in+progress+sliding+sign).

Keeping a "clean" working environment will help you manage your stress, but it will also help you with problem-solving.
If you are stuck on a difficult technical problem all day, the likelihood is that you will remain stuck.
If you can take a real break from technical work, you will give your mind a chance to process your thoughts while you are away.

### Stick to regular work times

At Beautiful Canoe our office hours are 09:30--17:30.
It is tempting to let work hours drift when you work at home, either by allowing yourself to spend time on other things during the working day, or by working unpaid overtime.
Of course, it's fine to take a break or to run some errands during the day, but as far as possible, stick to the normal working hours, when colleagues will be available to collaborate with you.

It will help your stress levels, and your sleeping patterns to be as strict as possible about working regular hours, and to take a full lunch hour during the day.
When you are not working, try not to think about work at all -- close all your work related apps and browser windows, and if you can, don't go into your physical working environment at all.

### Taking regular breaks

Make sure that you take regular breaks, ideally at least 5 minutes for every 90 minutes you work, as well as a full lunch hour.
If you find this difficult to manage, try setting an alarm on your phone or using [one of many](https://pomodoro-tracker.com/) online [Pomodoro timers](https://tomato-timer.com/).

## Further reading

* [GitLab all remote guide](https://about.gitlab.com/company/culture/all-remote/guide/)
* [Trello - How To Be A Happy And Productive Remote Worker](https://blog.trello.com/happy-productive-remote-worker)
* [Working out loud](https://www.collaborationsuperpowers.com/125-management-by-working-out-loud/)
