# How to triage issues

To *triage* is to determine the priorities in a situation.
In the context of software engineering, *issue triage* means to ensure that [every issue](https://gitlab.com/groups/beautifulcanoe/-/boards) contains enough information to be resolved by a developer.
This involves adding enough information to clarify a ~Bug or ~"Feature Request", accurately [labelling](/start-here/howto-use-labels.html) issues so that they can be searched and filtered, and assigning them to [milestones](https://gitlab.com/groups/beautifulcanoe/-/milestones) and developers.

In Beautiful Canoe, most issue triage is done by the CTO and @bc-bot, but any member of the company can triage issues.
As a minimum, every developer should take responsibility for triaging issues that they have raised.

## Levels of triage

We define three levels of triage:

* Level 1 (triage has been started):
    * Has a [type label](howto-use-labels.md#type-labels) applied.
    * Has a [priority label](howto-use-labels.md#priority-labels) applied.

* Level 2 (triage is partial):
    * Has steps to reproduce and has been ~Confirmed, if the type is ~Bug
    * Has a clear description and business case, and criteria for completion and QA testing, if the type is ~"Feature Request".

* Level 3 (triage is complete):
    * Has a [workflow label](howto-use-labels.md#workflow-labels).
    * Has a milestone set.
    * Has a developer assigned.

## Triaging issues

Initial triage involves (at least) adding appropriate labels, so that un-triaged issues can be discovered by searching for issues without any labels.

To start, pick an issue from [the `beautifulcanoe` board](https://gitlab.com/groups/beautifulcanoe/-/boards), [the `feed-quest` board](https://gitlab.com/groups/feed-quest/-/boards) or [the `traffic3d` board](https://gitlab.com/groups/traffic3d/-/boards) taking the older issues first, bearing in mind [Beautiful Canoe issue policies](https://gitlab.com/beautifulcanoe/devops/triage-bot/tree/main/policies).

Some questions to keep in mind:

* Do you understand what the issue is describing?
* What labels apply?
* How critical does it seem?
* Has the issue been raised specifically by the client (in which case, it should have higher priority)?
* Would the ~"Security Vulnerability" label be appropriate?
* Should it be made confidential? ~"Security Vulnerability" issues should *always* be confidential.

Apply each label that seems appropriate.

If the issue seems unclear, or you aren't sure which labels to apply, ask the person who raised the issue to clarify.
If that person who raised the issue has left the company, check with the CTO.
Make sure you check GitLab regularly for replies to your discussion comments.

Check for ~Duplicate issues.
Searching for some keywords in the issue should give you a list possible duplicates to consider.
Check both open and closed issues.
If an issue has been duplicated, mark *one* of the issues with ~"Wont Fix", leave a comment on it such as *Closing as duplicate of #N* and close it.
Before you close, make sure that any useful information in the closed issue is left as a comment in the issue you leave open.

Consider whether the issue is still valid.
Especially for older issues, a ~Bug may have been fixed since it was reported, or a ~"Feature Request" may have already been implemented.
You might want to search through commit messages in the `git log`, or a development, staging or production deployment of the code if you are unsure.

!!! Warning
    Labels relating to milestones, such as ~Missed:2019.1, are added automatically by @bc-bot -- please do not add them automatically.

## Issue triage best practices

We're enforcing some of the policies automatically in the [triage-bot](https://gitlab.com/beautifulcanoe/devops/triage-bot), using the @bc-bot user.
For more information about the automated triage, please see [the repository](https://gitlab.com/beautifulcanoe/devops/triage-bot).

### Take ownership of issues you've opened

Sort issues by **Author: your username** and close any issues which you know have been fixed or have become irrelevant for other reasons.
Label them if they're not labelled already.
Make sure that you reply to any comments on your issues.

### Standard priorities

In general:

* issues marked ~"Security Vulnerability" should be marked ~Priority:High.
* issues that affect the [accessibility of a product](https://www.w3.org/WAI/fundamentals/accessibility-intro/) should be marked ~Priority:High, because they prevent groups of users using the product.
* issues that have some user experience component should not be marked ~Priority:Low.
* issues marked ~Documentation should not be marked ~Priority:Low, and you should consider documentation to be equally important as code.

### Good first issues

Some issues have low priority but are easy to resolve.
These should be marked as ~"Good First Issue", to encourage developers who are new to the project to pick them up.

### New labels

If you notice a common pattern amongst various issues, feel free to suggest adding a new label the relevant [Slack channel](https://beautifulcanoe.slack.com/).

## Further reading

* [Managing the Deluge of Atom Issues](https://blog.atom.io/2016/04/19/managing-the-deluge-of-atom-issues.html)
* [How to be an open source gardener](https://words.steveklabnik.com/how-to-be-an-open-source-gardener)
