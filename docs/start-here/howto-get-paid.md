# How to get paid at Beautiful Canoe

Everybody likes money💰.

## The staff portal

To fill your timesheet, you have to go to the [Aston Staff Portal](http://www.aston.ac.uk/staff/hr/aston-staff-portal/).
From there you will be able to go to the [Aston Staff Portal login](https://my.corehr.com/pls/coreportal_astp/cp_por_public_main_page.display_login_page) where you will be able to login to access the portal.

## Adding a New Timesheet

After you login you will be presented with the portal with different functions on the left hand side.
You will want to click **Timesheets** to fill in your timesheet.

The timesheet page will display all your previous requests you have submitted, however if you have not submitted one before then this will be blank.
To submit a timesheet click the **+ New** blue button in the right hand corner of the timesheet history section.

![Portal screenshot](./figures/payCoreHR.png)

You will be presented with a pop-up box called **Add New Timesheet**.
From here you will want to go to the **Personal Details** section, click the pencil and paper symbol to the right of the **Appointment ID** section.

![Add new timesheet screenshot](./figures/payAddNewTimesheet.png)

From here another pop-up box will appear, select the relative option for your timesheet.

After that go to the **Work Undertaken** section and enter the work you have completed.
Start by filling in the **Date Worked** section then filling in the **Time Worked** sections using 24-hour military time (e.g. `09:00`, `13:30`).
If you work a full day, it is important that you enter the morning and afternoon as separate entries, that show that you had a lunch break.
If you don't do this, then it may lead to delays in your payment, as employers have a legal duty to ensure that staff have sufficient breaks.
This should also encourage you to have a proper lunch break :pizza:.

Then click the pencil and paper symbol under the **Pay Code** section and a pop-up box will appear.
Click the **Filter** button in the pop-up box to display the correct Pay Code for you.

![Filter pay code list screenshot](./figures/payFilterPayCodeList.png)

After you have filled in your timesheet, you will have to check the box under the **Declaration** section to confirm the information you are submitting is accurate.
After that click the **Submit Timesheet** button at the bottom of the page to submit your timesheet.

![Submit timesheet screenshot](./figures/paySubmitTimesheet.png)

You may want to double check your bank details and other information before your submit your timesheet just to see if all the information is correct.

The timesheet will now appear under your **My Timesheets** section of the staff portal.
This gives you information about your timesheet such as **Status** which tells you information such as if your timesheet as been approved or not.

## Payment Details

* Payslips need to be submitted AND approved before the 15th to be paid during the same month.
* If approved on time, you should get paid on the 25th of the month.
