# OWASP Top 10

## Injections

### MongoDB

#### Understanding the query language used by MongoDB

MongoDB queries are arrays (usually associative) and are vulnerable to attacks.
It is important to note that the queries are not declarative like SQL and instead are written procedurally which gives the hacker the ability to do more as they no longer call functions to do the job but instead get to write the functions that will query the DB.

[This website](https://docs.mongodb.com/manual/tutorial/query-documents/) shows how MongoDB queries can be carried out in different languages such as PHP, C#, Java...

#### Injection types and preventions

##### Stealing records

This form of injection gets records from a database by editing the query.
Developers often use the super global variable as apart of their query which is an issue because this variable can be edited by the user and therefore manipulate the query without permission.
By adding `[]` to the super global using the browser, the user can convert the super global into an array which can then be used.
The [Adldap2](https://adldap2.github.io/Adldap2-Laravel/#/) query builder comes with LDAP security and in the ACTSS project it has been imported.
This technology was used to carry out the queries within ACTSS which means that this project is secure against LDAP injections.
This is because the [Adldap2](https://adldap2.github.io/Adldap2-Laravel/#/) functions such as select and where escape the data  as a query for the DB.

##### Stealing records prevention

To prevent the user from injecting their own queries, the data they send can be imploded.
When it is imploded, if the data is an array, it will be converted into a string which means that the value cannot be a query but instead will be value.
So imploding stops the user from passing in their own array instead of a value which intern stops them from querying the database.

Example: `$SafeValue = implode(",", $array);`

##### Expose Database structure

A user can induce errors through the front end of the program and then use these error messages to gain knowledge of the database structure.
This information can then be used to manipulate the database as the capabilities of injections are limited without sufficient knowledge of the database's structure.
For example, the user can edit the names of the parameters to X which will return `Undefined: Parametername1` and now the user will know that parameter 1 is a column in the database and then they can use this parameter name to retrieve all the records under this field.

##### Expose Database structure prevention

Inducing errors to find out database information can also be prevented through the use of the `addslashes()` function in PHP which will stop the query from breaking even if the data that has been entered is invalid.

## Broken Authentication

[Source](https://www.owasp.org/index.php/Broken_Authentication_and_Session_Management)

Broken authentication is essentially where a user is allowed to enter a web application without having the correct credentials / privileges to be able to enter.
Broken authentication can be exploited in many different ways by malicious users by simply abusing small little overlooked details that the applications creators may have missed or not seen as being significant.

### How broken authentication can be abused

[Source](https://www.owasp.org/index.php/Top_10_2013-A2-Broken_Authentication_and_Session_Management)

#### Credential stuffing

Credential stuffing is where a malicious user has access to a list of "known" credentials which have been leaked previously in the past usually from big corporate databases which have been hacked.
They will then use some kind of automated tool / program which will just attempt to login to an application using these credentials until they finally manage to login with one of the stolen credentials.

#### Automated attacks

Automated attacks is not to be confused with credential stuffing as the difference is automated attacks are carried out without having any "known" credentials and the automated program using random username / password combinations in order to try and brute force it's way into a web application.

#### Default / common passwords

Malicious users will commonly have a list of very typically used passwords or even stock / default username/password combinations, in which they can try and more commonly than you'd think they will be able to gain access to an application this way.

#### Missing / ineffective multi factor authentication

Typically the more insecure web applications will not have any sort of multi factor authentication in place, for example no email authentication after logging in or a biometric system of some sort. which allows any malicious user to be able to brute force their way in with credential stuffing or automated attacks.

### How to prevent broken authentication

[Source](https://www.owasp.org/index.php/Top_10_2013-A2-Broken_Authentication_and_Session_Management)

#### Credential stuffing

Require users to change their password after a certain time interval (maybe 1 - 3 months) and it must not be the same as a previous password, this will ensure that if any users credentials have been leaked in the past through a major database leak they will still be safe on your application as their passwords will not be the same.
Multi-factor authentication can also be set up to help solve the issues.

#### Automated attacks

Automated attacks can be prevented by only allowing users between 3 - 5 login attempts roughly every 30 minutes or so this will highly discourage users from attempting to brute force their way in through automated attacks and it will flag up to an administrator that someone may be potentially attempting automated attacks.

#### Default / common passwords

This can be a simple fix, when users go to create accounts on your systems ensure that they must follow certain rules around creating passwords.
Usually the standards could be along the lines of:

* Must be minimum of 8 characters in length
* Must contain a combination and alphabetic, numeric and non-alphabetic characters
* Must contain a mix of upper and lower case characters

If there are already accounts that have been created before these rules have been enforced the administrators could go in and check through their users passwords and if any passwords they find are either default / commonly used passwords or do not follow the stated rules above they can send that user a message and request a password change.

#### Missing / Ineffective multi factor authentication

Adding a multi factor authentication will essentially be like having a second line of defence if a malicious users manages to get hold of a set of valid credentials.
Multi factor authentication could be added in a few ways such as having an authentication code being sent to a mobile device which the legitimate user has set-up, this will make it so the account is impossible to access without being able to use that device. Or the standard email verification will suffice as the malicious user will need access to the corresponding email address.

## Sensitive information

According to ICO ([information commission office](https://ico.org.uk/)), sensitive information is any data about an individual's:

* race
* ethnic origin
* politics
* religion
* trade union membership
* genetics
* biometrics (where used for ID purposes)
* health
* sex life
* sexual orientation

Any data that falls under this category has to be:

* stored only if necessary
* encrypted and not stored as plain text
* transmitted using secure network protocols  (especially if data is transmitted as plain text)
* deleted upon the users request

Following these guidelines reduces the risk of data exposure and complies with the general data protection regulation.

## ACTSS Security

### LDAP Injections

Sources:

* [ADLDAP2 DOCUMENTATION](https://github.com/Adldap2/Adldap2/blob/v8.0/docs/query-builder.md)
* [OWASP LDAP INJECTION PREVENTION CHEAT](https://cheatsheetseries.owasp.org/cheatsheets/LDAP_Injection_Prevention_Cheat_Sheet.html)

The [Adldap2](https://adldap2.github.io/Adldap2-Laravel/#/) query builder comes with LDAP security and in the ACTSS project, it has been imported.
This technology was used to carry out the queries within ACTSS which means that this project is secure against LDAP injections.
This is because the [Adldap2](https://adldap2.github.io/Adldap2-Laravel/#/) functions such as select and where escape the data (according to this [page](https://github.com/Adldap2/Adldap2/blob/v8.0/docs/query-builder.md)).
If the fields  are escaped, LDAP injections are no longer possible as escaping the data is the primary defence against this type of attack (according to this [page](https://cheatsheetseries.owasp.org/cheatsheets/LDAP_Injection_Prevention_Cheat_Sheet.html)).

Here is an example of where [Adldap2](https://adldap2.github.io/Adldap2-Laravel/#/) is used carry out a query `where($property, $credentials['email'])` and this query will be safe against injections.

However, the `rawfilter()` function does not escape the data in the query.
Therefore LDAP injections can be carried out if this function is used.
The vulnerability is made worse if the inputs are not manually escaped according to this [page](https://github.com/Adldap2/Adldap2/blob/v8.0/docs/query-builder.md).
After looking through the code, I found that the `rawfilter()` function has not been used within ACTSS, making the project secure.

#### To Maintain this security

1. **USE ADLDAP2 TO CARRY OUT THE QUERIES**
1. **DO NOT USE THE USER'S INPUT IN THE ADLDAP2 `rawfilter()` FUNCTION**

## Cross Site Scripting

[Source](https://www.owasp.org/index.php/Cross-site_Scripting_(XSS))

Cross-Site Scripting (XSS) attacks are a type of injection, in which malicious scripts are injected into otherwise benign and trusted websites.
XSS attacks occur when an attacker uses a web application to send malicious code, generally in the form of a browser side script, to a different end user.

### Forms of XSS

#### Reflected XSS

This is where an application or API contains unescaped user input as part of a HTML output.
This essentially allows a user to be able to open HTML tags within an input box and be able to tell the website what to run / do or even redirect other users which can be extremely harmful.

#### Stored XSS

Stored XSS is also where unsanitised or escaped user input is entered into a field in the website, however this gets stored by the website and can be run at any later date simply by a user opening that website.
This is a big problem as it can potentially effect a large amount of users over time.

#### DOM XSS

This is a type of scripting which targets pages with attacker controllable data such as single-page applications, API's and Javascript frameworks.
Malicious users try to abuse developers who design applications to send attacker-controllable data to unsanitised API's.

### Preventing XSS

#### Frameworks

One great way of protecting your application from XSS style attacks is by using frameworks and sticking to their "way" of creating applications.
Not every framework will cover all forms of attacks so it is important to know what your framework does protect against.

#### Escaping

Escaping certain characters is so important to ensuring that your application is safe from XSS attacks.
This can very simply be done as well to ensure that user input for these certain characters are replaced with their sanitised versions.

* `& --> &amp;`
* `< --> &lt;`
* `> --> &gt;`
* `" --> &quot;`
* `' --> &#x27;`
* `/ --> &#x2F;`

For more information refer [here](https://cheatsheetseries.owasp.org/cheatsheets/DOM_based_XSS_Prevention_Cheat_Sheet.html#rule-1---html-escape-then-javascript-escape-before-inserting-untrusted-data-into-html-subcontext-within-the-execution-context)

#### DOM XSS

Applying context-sensitive encoding when modifying the browser document on the client side acts against DOM XSS.
When this cannot be avoided, similar context sensitive escaping techniques can be applied to browser APIs.

For more information refer [here](https://cheatsheetseries.owasp.org/cheatsheets/DOM_based_XSS_Prevention_Cheat_Sheet.html)

### Vulnerabilities in the technologies used

| # | Technology | Security vulnerabilities                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Reduce risks against vulnerabilities                                                                                                                                                                                                                                                             |
|---|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | laravel    | https://www.quora.com/What-are-the-most-serious-security-vulnerabilities-of-Laravel-PHP-framework  Laravel is focused when it comes to security and lacks in vulnerabilities. After doing some research, I was unable to find any significant laravel security issues. This is mainly due to the fact that it is regularly updated  and mature and as a result, the OWASP top 10 security vulnerabilities are avoided.   However, not using the framework can make the benefits of its  built-in security measures  useless which intern makes the application vulnerable. For example, not using the query builder to write SQL queries will make the framework’s protection against SQL query injections irrelevant.                                                                                                                    |  - Make use of the built in security features and follow the guidelines.  - Constantly update the framework.                                                                                                                                                                                     |
| 2 | MySql      |                        https://www.cvedetails.com/vulnerability-list/vendor_id-185/product_id-316/Mysql-Mysql.html                                           MySql is very popular so attackers are constantly working on             new ways of infiltrating the database via vulnerabilities.             Unfortunately, they are often successful and websites such as             common vulnerabilities and exposure post these  vulnerabilities.             However, Mysql does release patches very frequently so             vulnerabilities are a risk for only a short period of time unless             Mysql is not updated.                                           Mysql is password protected. If the password is too weak or is             used on other services it risks being discovered as brute force             can be used to expose weak passwords and leaks are made more             likely if it used often.                                                                                                     | - Watch the mysql CVE  to get alerted about very high-risk vulnerabilities.  - Avoid using weak passwords and update passwords regularly.                                                                                                                                                        |
| 3 | PHP        |  https://www.cvedetails.com/vendor/74/PHP.html  PHP is one of the most commonly used server-side languages and the Laravel framework is actually built using PHP. This popularity makes it a key target for hackers and new vulnerabilities are discovered constantly.       The CVE has data about PHP which shows the different attacks that PHP is vulnerable to. Dos, Code execution and overflow attacks make up 90% of the vulnerabilities whereas XSS, SQL injections and other attack types are less common.         PHP releases new security for most of its versions. So, if you are using PHP 5.2 as opposed to a newer version such as 7.2, you will still get security. However, over time, the focus will shift because as new versions get released, PHP’s developers will not be able to maintain very old PHP versions. | - Always add measures to prevent the risk created by the top 3 vulnerabilities (which make up 90% of attacks against PHP).  - Always install the security updates from PHP.  - Over time, start to move to newer versions of PHP and try not to use versions that are 2 or more years old. |
