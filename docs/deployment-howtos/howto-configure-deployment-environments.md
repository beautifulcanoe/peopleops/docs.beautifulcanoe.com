# How to configure deployment environments

When deploying to an environment, you will need to ensure that configuration files are set specifically for that environment.
For example, you may need to point to a new database or connect to a different LDAP server.
The Beautiful Canoe [deployment framework](https://gitlab.com/beautifulcanoe/devops/deployment) enables projects to manage this process.

## Adding the deployment framework to your repo

You should add the framework to your repository as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

```shell
git submodule add git@gitlab.com:beautifulcanoe/devops/deployment.git
```

This will create a new file called `.gitmodules`.
This is a configuration file that stores the mapping between the project’s URL and the local subdirectory you’ve pulled it into:

```json
[submodule "deployment"]
    path = deployment
    url = ../deployment.git
```

You then need to commit the change you just made, and push it in the usual way:

```shell
git commit -m "Add deployment module."
git push origin FEATURE_BRANCH
```

## The `project.sh` file

Most of your project-specific information will live in a Bash script called `project.sh` which should be located in the root of your repository.
The framework repo has some [example files](https://gitlab.com/beautifulcanoe/devops/deployment/tree/main/examples) that are worth consulting before you go any further.

Be aware that at the top of `project.sh` you need a variable called `PROJECT_TITLE`.
This is the directory that git will clone into when your project is cloned on one of our deployment servers.
For example, if your git endpoint is `git@gitlab.com:beautifulcanoe/projects/MYPROJECT.git` then `PROJECT_TITLE` should be set to `MYPROJECT`.
If you rename your project and move its git endpoint, you also need to raise an MR to change `PROJECT_TITLE`.

## Deployment types

The deployment framework provides three environment types: *live*, *demo* and *dev*.
For legacy reasons, the framework calls staging environments `demo`, production environments `live` and all other environments `dev`.

These environments are signalled to your `project.sh` script by calling the `set_{live|demo|dev}` function.
Each function should set the correct variables needed to deploy, which `generate_config_files` should then use to generate the files.

## Values that differ between deployments

The values for variables (e.g. database username/password) should **never** be hard-coded in your deployment scripts, for security reasons.
Instead, use [GitLab's Variables](https://docs.gitlab.com/ce/ci/variables/) within your project. Setting a GitLab variable of `LIVE_DB_USER` as `foobar` will be resolved automatically when your deployment script is executed.
In the example below, `set_live` will set the db config user to `foobar`, even though the variable isn't set in the script.
GitLab handles the population of (project-defined) variables through it's [GitLab Runner](https://docs.gitlab.com/runner/).

### Example

```shell
function set_live {
    DB_CONFIG[user]="$LIVE_DB_USER"
}

function set_demo {
    DB_CONFIG[user]="$DEMO_DB_USER"
}

function set_dev {
    DB_CONFIG[user]="$DEV_DB_USER"
}

function generate_config_files {
    sed -i "s/%PLACEHOLDER%/${DB_CONFIG[user]}/g" config.xml
}
```

`generate_config_files` is executed regardless of environment, so that function shouldn't do any checking as to which environment it is using.
Values should be inserted into an environment-independent variable (`$DB_CONFIG[user]` above).
