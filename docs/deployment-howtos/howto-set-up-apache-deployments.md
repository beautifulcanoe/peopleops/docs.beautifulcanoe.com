# How to set up Apache deployments

## Directory structure

Usually, each project is deployed into the home directory of the user.
This location should be echoed by the `get_deployment_location()` function.

Each build is stored in the project's home directory.
For an example project with the title `foo`, a deployment location of `$HOME/` and a build ID of 5, the directory structure is as follows:

* `~/foo/1`
* `~/foo/2`
* `~/foo/3`
* `~/foo/4`
* `~/foo/5`
* `~/public_html` -> `~/foo/5/`

If `target_subdirectory` echoes a subdirectory name, that will be appended to the symlink for public_html.
For example, if target_subdirectory prints `bar`:

* `~/foo/1`
* `~/foo/2`
* `~/foo/3`
* `~/foo/4`
* `~/foo/5`
* `~/public_html` -> `~/foo/5/bar`

## Clearing down deployments

As you may notice, `~/foo/` will get full of deployments over time.
This can be rectified by clearing down old versions after deployment.
To do this, make a function call to `clear_old_apache_deployments`.
Arguments must be passed in the format: `{number of deployments to keep} {location of deployment}`.
Location of deployment should be `~/foo/` in the example above.

After executing: `clear_old_apache_deployments 3 ~/foo`:

* `~/foo/3`
* `~/foo/4`
* `~/foo/5`
* `~/public_html` -> `~/foo/5/bar`
