# How to update the deployment framework

```shell
cd <YOUR PROJECT DIRECTORY>
cd deployment
git pull
git checkout <TAG_NAME_HERE>
cd ../
git add deployment
git commit -m "Updated deployment to latest version"
git push
```

In the above command, `<TAG_NAME_HERE>` will need replacing with the tag name provided by the deployment script maintainer (e.g. v1.0).

Once updated, you should run this through the dev -> demo -> live process as normal.
Test that the updated scripts work for your environments, as newer versions may introduce issues with some projects.

Generally, you should be emailed if there are any changes to the deployment scripts.
If you make changes to these scripts yourself, please email the [Beautiful Canoe mailing list](http://lists.aston.ac.uk/pipermail/beautifulcanoe/), and make an announcement on [Slack](https://beautifulcanoe.slack.com) to inform everyone after you have tested them thoroughly.
Project leads should then handle the update to the latest version if needed.
These scripts are used by all projects so please be very careful when updating them.
