# How to configure git

## .gitconfig files

Git stores its configuration in a file called `~/.gitconfig`.
The config file contains a number of settings, which are grouped together.
For example:

```yaml
#
# This is the config file, and
# a '#' or ';' character indicates
# a comment
#

# Core variables
[core]
    # Default text editor.
    editor = subl
```

Here, `[core]` is a group of settings and each line underneath the group name, that isn't a comment, is a single setting.

As well as setting configuration options, your `.gitconfig` file can be used to create new commands.
These are called aliases - they are short names for more complicated operations.
For example:

```yaml
[alias]
    # Checkout, with less typing.
    co = checkout
```

## Beware

If you search online you will find a large number of example git config files.
Some of these are useful, but some will be for a different version of git than the one you are running.
Some will contain very powerful features that might cause you problems -- for example automatically creating and deleting commits and branches.
In general, if an example setting isn't listed here, and you don't fully understand what it does, it's best not to use it.

## An example .gitconfig file

Most developers find that their `.gitconfig` file gets more complex over time, as they slowly learn git in more depth.
The example file below is a good set of defaults to start you off.
You should definitely set the `core.editor` variable to your favourite editor.
Apart from that, we recommend that you leave this file as-is, until you feel comfortable that you understand any changes you might want to make.

```yaml
[core]
    # Set this to your favourite text editor, e.g. vim, emacs, sublime...
    editor = nano

[merge]
    # Use meld http://meldmerge.org/ as a merge tool.
    tool = meld
    conflictstyle = diff3

[grep]
    # Add line numbers to the output of `git grep`.
    lineNumber = true

[help]
    # When I make a typo in a git command, guess what I meant and carry on.
    # e.g. 'git stattus' should run 'git status'.
    autocorrect = 1

[pager]
    # When the output of a git command runs over more than one console, show
    # me which branch I'm on.
    show-branch = true
    status = true

[status]
    # When I run 'git status' tell me about submodules.
    submodulesummary = true

#
# Set some default colours.
#

[color]
    branch = auto
    diff = auto
    status = auto

[color "branch"]
    current = yellow reverse
    local = yellow
    remote = green

[color "diff"]
    meta = yellow bold
    frag = magenta bold
    old = red bold
    new = green bold

[color "status"]
    added = yellow
    changed = green
    untracked = cyan

#
# Aliases create new commands. Use them as if they were a built-in git command
# e.g. 'git tree-log'.
#

[alias]
    # Checkout with less typing.
    co = checkout

    # tree-log is a much more readable version of 'git log'.
    tree-log = log --color --graph --oneline --decorate

    # How many commits are in this branch, but not in develop?
    howmany = rev-list --count develop..

    # List any text in the repository which looks like a task.
    tasks = grep -EI "TODO|todo|FIXME|fixme"

    # Show the changes that are currently unstaged (i.e. not going to be
    # committed).
    dc = diff --cached

    # Show the changes that are currently staged (i.e. are going to be
    # committed).
    ds = diff --staged

    # Show a list of branches and their commits.
    sb = show-branch

    # Find text in any commit in the repo history.
    grep-all = !"git rev-list --all | xargs git grep '$1'"

    # Show who contributed which changes.
    who = shortlog -s --

    # Lookup full name and email for any commit author.
    whois = "!sh -c 'git log -i -1 --pretty=\"format:%an <%ae>\n\" --author=\"$1\"' -"

    # Show who contributed, in descending order by number of commits.
    whorank = shortlog -sn --no-merges

    # Stash aliases. Stashing is useful when you want to move between branches,
    # and you don't want to take your unstaged changes with you.
    save = !git stash save
    pop = !git stash pop
```

## Adding a default commit message template

To help you apply [good practices in writing commit messages](https://chris.beams.io/posts/git-commit/) it's a good idea to change the default commit message template.
If you are using Linux or MacOS, open your favourite editor at `~/.git_commit_msg.txt`, or if you are using Windows open `~/.GIT_COMMIT_MSG`, and add the following text:

```shell
# (If applied, this commit will...) <subject> (Max 50 char)
# E.g. FIX project title order
#      Commit hashes are hyperlinks
#      Add unit tests for auth subsystem
# |<----  Using a Maximum Of 50 Characters  ---->|

# Leave one blank line between the commit subject and body.

# Explain why this change is being made
# |<----   Try To Limit Each Line to a Maximum Of 72 Characters   ---->|

# Provide links or keys to any relevant tickets, articles or other resources
# Use issues and merge requests' full URLs instead of short references,
# as they are displayed as plain text outside of GitLab

# --- COMMIT END ---
# --------------------
# Remember to:
#    1. Capitalize the subject line
#    2. Use the imperative mood in the subject line
#           Good: Refactor subsystem X for readability
#           Good: Update getting started documentation
#           Good: Remove deprecated methods foo and bar
#           Good: Add unit tests for auth subsystem
#           Bad: Fixed bug with Y
#           Bad: Changing behaviour of X
#           Bad: More fixes for broken stuff
#           Bad: Sweet new API methods
#    3. Do not end the subject line with a period
#    4. Subject must contain at least 3 words
#    5. Separate subject from body with a blank line
#    6. Commits that change 30 or more lines across at least 3 files must describe these changes in the commit body
#    7. Do not use Emojis
#    8. Use the body to explain what and why vs. how
#    9. Can use multiple lines with "*" for bullet points in body
#
# For more information: https://chris.beams.io/posts/git-commit/
# --------------------
```

Save the file and run this command if you are on Linux of MacOS:

```shell
git config --global commit.template ~/.git_commit_msg.txt
```

or, on Windows:

```shell
git config --global commit.template ~/.GIT_COMMIT_MSG
```

Now, whenever you run `git commit` you should see the text above.
