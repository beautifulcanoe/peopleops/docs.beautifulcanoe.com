# How to Checkout two git branches simultaneously

You may want to have two branches of the same Git project checked out at the same time, routed to the same database.

The most useful reason for this is if you have two branches in the same project running on two different versions of PHP.
We are using an example of checking out `main` and `develop`.

## Create  Worktree

First, we need to create a git worktree. Go to where your current git project is saved and checkout to `main`.
Then run:

```shell
git worktree add  ../project_name-develop develop
```

With ` ../project_name-develop` is the path of the new project.
`develop` is the branch you want to checkout.
Now you have two branches of the same git project saved on your machine.

Please see resource: [Git Worktree Documentation](https://git-scm.com/docs/git-worktree), for more information on Git Worktrees.

## Homestead.yaml

Add the new directory to your `Homestead.yaml` file, you don't need to edit the previous mapping, only add the new one.
Your file should look like:

```yaml
folders:
    ...
    - map: .../project-name
      to: /home/vagrant/project-name
    - map: .../project-name-develop
      to: /home/vagrant/project-name-develop

sites:
    ...
    - map: project-name.test
      to: /home/vagrant/project-name/public
    - map: project-name-develop.test
      to: /home/vagrant/project-name-develop/public

databases:
    ...
    - project-database
```

Notice we make no edit to databases, that is because both branches still use the same database.

### Changing PHP version

With some projects they may require sites to use different versions of PHP, if so, follow these steps (using the example of changing the `main` branch to PHP 5.6 and `develop` to 7.2).

Homestead version 6 and above (you should be running homestead 7) allows you to assign php version to the sites automatically.

```yaml
sites:
    ...
    - map: project_name.test
      to: /home/vagrant/project_name/public
      php: "5.1"
    - map: project_name-develop.test
      to: /home/vagrant/project_name-develop/public
      php: "7.1"
```

PHP versions 5.6, 7.0, 7.1 and 7.2 are normally automatically installed in Homestead.
If you need a different version then installed using PHPBrew (Information on how to do this can be found here [PHPBrew documentation](../php-howtos/howto-setup-phpbrew.md)).

## Edit hosts file

Add the new site to your `hosts` file (`/etc/hosts` on Debian-based systems):

```yaml
...
192.168.56.10    project_name-develop.test
```

### Re-provision Vagrant

It is important you remember to re-provision Vagrant using:

```sh
vagrant reload --provision
```

## Setting up in project directories

Make sure you copy the the .env file from the `main` project to the `develop` project.

Re-run:

```shell
composer install
php artisan migrate
php artisam db:seed
```

## Testing version

To test which version of php is running, simply put `<?p phpinfo() >` on an easily accessible view and visit the webpage.

## Command line PHP

Changing the version of PHP in `Homestead.yml` changes the PHP that serves each site, but the command-line version of PHP will always be the system default.
If you need to change the command-line PHP, use `update-alternatives`:

```shell
$ sudo update-alternatives --config php
There are 5 choices for the alternative php (providing /usr/bin/php).

  Selection    Path             Priority   Status
------------------------------------------------------------
  0            /usr/bin/php7.3   73        auto mode
  1            /usr/bin/php5.6   56        manual mode
  2            /usr/bin/php7.0   70        manual mode
  3            /usr/bin/php7.1   71        manual mode
  4            /usr/bin/php7.2   72        manual mode
* 5            /usr/bin/php7.3   73        manual mode

Press <enter> to keep the current choice[*], or type selection number: 1
update-alternatives: using /usr/bin/php5.6 to provide /usr/bin/php (php) in manual mode

$ php --version
PHP 5.6.40-8+ubuntu18.04.1+deb.sury.org+1 (cli)
Copyright (c) 1997-2016 The PHP Group
Zend Engine v2.6.0, Copyright (c) 1998-2016 Zend Technologies
    with Zend OPcache v7.0.6-dev, Copyright (c) 1999-2016, by Zend Technologies
```

And you will be given the option to change PHP version.

## Further reading

* [Git Worktrees](https://git-scm.com/docs/git-worktree)
