# How to set-up Git autocomplete on MacOS

When a developer is working on a project that has multiple contributions it can become tedious to manually type out branch names everytime.
Git autocomplete resolves this issue by allowing for branches to be completed by using the tab button to complete the branch you were writing out.

## Installing Git autocomplete

First, open up the terminal in your mac and run the `curl` command

```shell
curl
https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash
```

No need to worry about what directory you're in when you run this as your home directory(~) is used with the target.

### Adding 'execute if exists' code

Open your Bash profile with the following command

```shell
open ~/.bash_profile
```

And add the following  line :

```shell
test -f ~/.git-completion.bash && . $_
```

Once added, save and close your Bash file

### Adding permissions

After the previous steps have been completed the following command should be executed

```shell
chmod u+x ~/.git-completion.bash
```

This is needed to grant yourself the necessary permissions:

`chmod` is the command that modifies file permissions.

`+` u means the user that owns the file, by default its creator, i.e. you

`+` means set/activate/add a permission

`x` means execute permission, i.e. the ability to run the script

### Running autocomplete

Once the previous steps have been completed, quit terminal and reopen it, you should then be able to use tab to auto complete git commands.
